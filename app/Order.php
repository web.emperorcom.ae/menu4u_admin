<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    //
    protected $fillable = [
        'id',
        'comment',
        'order_status_id',
        'address_id',
        'phone_id',
        'email_id',
        'user_id',
        'created_by_user_id',
        'updated_by_user_id'
    ];
}
