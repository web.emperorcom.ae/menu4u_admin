<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VariantOption extends Model
{
    //
    protected $fillable = [
        'id',
        'value',       
        'variant_id'
    ];

    public function variant()
    {
        return $this->belongsTo('App\Variant');
    }
}
