<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Measurement extends Model
{
    //
    protected $table = 'measurement';

    protected $fillable = [
        'id',
        'name',
        'short_name',
        'unit',
        'step'
    ];


    public function attribute()
    {
        return $this->hasMany('App\Attribute');
    }
}
