<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator,Response;
use App\Country;

class CountryController extends Controller
{
    //

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // 
        $data = Country::orderBy('id', 'DESC')->get();
    

        return view('country.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $country = Country::find($id);


        return view('country.edit',compact( 'country'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {       
        //
        $this->validate($request, [
            'name' => 'required',
            'alpha2' => 'required'
        ]); 
        
        $input = $request->all();
        
        $country = Country::find($id);

        $updata = [ 
            'name'        => $input['name'],
            'alpha2' 	  => $input['alpha2'],
            'alpha3' 	  => $input['alpha3'],
            'region' 	  => $input['region'],
            'sub_region'  => $input['sub_region'],        
            'dial_code'   => $input['dial_code']
            ];
        
        $country->update($updata);
               
        return redirect()->route('countries.index')
                        ->with('success','Country updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //        
        $country = Country::find($id);

        $country->delete(); 

        return redirect()->route('countries.index')
                        ->with('success','Country deleted successfully');

    }

}
