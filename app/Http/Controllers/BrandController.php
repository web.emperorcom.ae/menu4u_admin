<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Brand;
use Validator,Response,File;

class BrandController extends Controller
{   
   
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $brand = Brand::orderBy('id','DESC')->get();

        return view('brands.index',compact('brand'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('brands.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'name' => 'required',
            'image_path' => 'file|image|mimes:jpeg,png,jpg,gif,svg|max:2048'    
        ]);
    
        $input = $request->all(); $img = null;
        if ($request->hasfile('image_path')) {

            $file = $request->file('image_path');
            $response = $this->postImage($_FILES);

            if(isset($response['status']) && $response['status'] == 'SUCCESS'){
                $img = $response['data']['url'];
            }else{
                // error
            }        
        } 

        $lastId = Brand::orderBy('id', 'desc')->first()->id;

        Brand::create([ 
            'id'    => (int) $lastId + 1,
            'name'  => $input['name'],
            'description' => $input['description'],
            'is_blocked'  => 0,
            'image' => $img,          
            'address_id' => null
            ]);
    
        return redirect()->route('brands.create')
                        ->with('success','Brand created successfully');
    }

    public function postImage($file){

        $ch = curl_init();
        
        $filePath = $file['image_path']['tmp_name'];
        $fileName = $file['image_path']['name'];
        $type = $file['image_path']['type'];
        $data = array('image' => curl_file_create($filePath, $type, $fileName));         
        
        curl_setopt($ch, CURLOPT_URL, 'https://api.menu4u.ae/api/image');
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,TRUE);
        $response = curl_exec($ch);    
        $err = curl_error($ch);
        curl_close($ch);

        if ($err) {
          return json_decode($err, true);
        } else {
          return json_decode($response, true);
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
         $brand = Brand::find($id);

        return view('brands.edit',compact( 'brand'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         //
         $this->validate($request, [
            'name' => 'required',      
            'image_path' => 'file|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]); 
        
        $input = $request->all(); $img = null;

        if ($request->hasfile('image_path')) {

            $file = $request->file('image_path');
            $response = $this->postImage($_FILES);

            if(isset($response['status']) && $response['status'] == 'SUCCESS'){
                $img = $response['data']['url'];
            }else{
                // error
            }        
        } 
        
        $brand = Brand::find($id);
        $updata = [ 
            'name'  => $input['name'],
            'description' => $input['description'],            
            'image' => $img? $img : $brand->image
            ];
        $brand->update($updata);
       
               
        return redirect()->route('brands.index')
                        ->with('success','Brand updated successfully'); 
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //        
        $brand = Brand::find($id);  

        $brand->delete(); 

        return redirect()->route('brands.index')
                        ->with('success','Brand deleted successfully');

    }

   
}
