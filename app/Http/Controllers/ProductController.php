<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Category;
use App\Brand;

use Redirect;
use DB;
use Image;
use Carbon\Carbon;
use Validator,Response,File;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $products = DB::table('products as p')
                    ->leftJoin('categories as c', 'p.category_id', '=', 'c.id')
                    ->leftJoin('brands as b', 'p.brand_id', '=', 'b.id')
                    ->leftJoin('images as im', 'p.default_image_id', '=', 'im.id')
                    ->select('p.id', 'p.name', 'p.price', 'p.min_price', 'im.url as image', 'p.created_at',
                        'c.name as category', 'b.name as brand')
                    ->orderBy('id', 'DESC')
                    ->get();


        return view('product.index',compact( 'products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // Categories
        $categories = DB::table('categories as child')
            ->leftJoin('categories as parent', 'child.category_id', '=', 'parent.id')
            ->select('child.*', 'parent.name as parent')   
            ->where('child.category_id','<>', 0)        
            ->orderBy('child.id', 'DESC')->get();

        $brand = Brand::all()->pluck('name', 'id')->toArray();  

        $countries = DB::table('countries')->orderBy('name', 'ASC')
                        ->pluck('name', 'id')->toArray();

        $measurement = DB::table('measurement')->orderBy('name', 'ASC')
                        ->pluck('name', 'id')->toArray();

        $variants = DB::table('variants')
                        ->orderBy('name', 'ASC')
                        ->pluck('name', 'id')->toArray();

        $attributes = DB::table('attributes')->orderBy('id', 'desc')
                        ->pluck("name", "id")->toArray(); 

        $products = DB::table('products as p')
                    ->select('p.*')         
                    ->orderBy('p.name', 'ASC')->get();     

        return view('product.create',compact( 'categories', 'brand', 'products', 'countries', 'measurement', 'variants', 'attributes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'name' => 'required',
            'price' => 'required', 
            'description' => 'required',  
            'category_id' => 'required',        
            'image' => 'required|file|image|mimes:jpeg,png,jpg,gif,svg|max:2048'                       
        ]); 
        
        $input = $request->all();

        ####### Add Product ######
        $img = null;
        if ($request->hasfile('image')) {

            $file = $request->file('image');
            $response = $this->postImage($_FILES);

            if(isset($response['status']) && $response['status'] == 'SUCCESS'){
                $img = $response['data']['id'];
            }else{
                // error
            }        
        } 
        $lastId = Product::orderBy('id', 'desc')->first()->id;
       
        $in_prod = [
                'id'          => (int) $lastId+1,
                'name'        => $input['name'],
                'description' => $input['description'],
                'price'       => $input['price'],
                'min_price'   => $input['min_price']?$input['min_price']: 0,
                'barcode'     => $input['barcode']?$input['barcode']: null,
                'measurement_id' => $input['measurement_id']?$input['measurement_id']: 0,
                'category_id' => $input['category_id'],
                'country_id'  => $input['country_id']?$input['country_id']: 0,
                'brand_id'    => $input['brand_id']? $input['brand_id']: 0,
                'default_image_id' => $img,
                'is_blocked'  => $input['is_blocked'],
                ];

        $product = Product::create($in_prod);

        ## Update IMAGE - PRODUCTID ##
        if($img > 0){
            DB::table('images')->where('id', $img)->update(['product_id' => $product->id]);
        }

        ####### Product Options ######
        if($product){  
            $lastId = DB::table('product_options')->orderBy('id', 'desc')->first()->id;

            $in_prod_opt = [
                'id'            => (int) $lastId+1,
                'sku'           => $input['sku']?$input['sku']: null,                
                'price'         => $input['price'],
                'count'         => $input['count'],
                'reserved_count'=> $input['reserved_count']?$input['reserved_count']: 0,
                'product_id'    => $product->id,
                'variant_id'    => $input['variant_id']?$input['variant_id'] : null,
                'variant_option_id' => $input['variant_option_id']?$input['variant_option_id']: null,
                'created_at'    => \Carbon\Carbon::now(), # new \Datetime()
                'updated_at'    => \Carbon\Carbon::now()  # new \Datetime()
            ];
            DB::table('product_options')->insert($in_prod_opt);
        }

        ####### Product Variants ######
        if(isset($input['variant_id']) && $input['variant_id'] > 0){ 

            $in_prod_var = [               
                "product_id"    => $product->id,
                "variant_id"    => $input['variant_id'],
                "created_at"    => \Carbon\Carbon::now(), # new \Datetime()
                "updated_at"    => \Carbon\Carbon::now()  # new \Datetime()
            ];
            DB::table('product_variants')->insert($in_prod_var);
        }

        ####### Product Attributes ######
        if(isset($input['attribute_id']) && !empty($input['attribute_id'])){   
            $lastId = DB::table('product_attributes')->orderBy('id', 'desc')->first()->id;
            foreach($input['attribute_id'] as $key => $value){
                if(!$value){continue;}
                
                if(isset($input['attr_value'][$key]) && $input['attr_value'][$key] != ''){
                    $lastId = (int) $lastId+1;
                    $in_prod_attr = [
                        "id"            => $lastId,
                        "product_id"    => $product->id,
                        "attribute_id"  => $value,
                        "value"         => $input['attr_value'][$key]? $input['attr_value'][$key]: null,
                        "created_at"    => \Carbon\Carbon::now(), # new \Datetime()
                        "updated_at"    => \Carbon\Carbon::now()  # new \Datetime()
                    ];
                    DB::table('product_attributes')->insert($in_prod_attr);
                }
            }
            
        }
               
        return redirect()->route('products.index')
                        ->with('success','Products created successfully');
    }

    

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   

        $product = DB::table('products as p')
                    ->leftJoin('product_options as po', 'po.product_id', '=', 'p.id')
                    ->leftJoin('images as im', 'p.default_image_id', '=', 'im.id')
                    ->select('p.*','im.url as image', 'po.sku as skucode', 'po.count','po.reserved_count','po.variant_id', 'po.variant_option_id')   
                    ->where('p.id','=', $id)
                    ->first();

        $productvariants = DB::table('product_variants as pv')
                    ->leftJoin('products as p', 'pv.product_id', '=', 'p.id')
                    ->select('pv.*')   
                    ->where('pv.product_id','=', $id)
                    ->first();

        $productattributes = DB::table('product_attributes as pa')
                    ->leftJoin('products as p', 'pa.product_id', '=', 'p.id')
                    // ->join('attributes as a', 'pa.attribute_id', '=', 'a.id')
                    ->select('pa.attribute_id', 'pa.value','pa.id as prod_attr_id')   
                    ->where('pa.product_id','=', $id)
                    ->orderBy('pa.attribute_id', 'DESC')
                    ->get();
        //dd($productattributes);
        // Categories
        $categories = DB::table('categories as child')
            ->leftJoin('categories as parent', 'child.category_id', '=', 'parent.id')
            ->select('child.*', 'parent.name as parent')   
            ->where('child.category_id','<>', 0)        
            ->orderBy('child.id', 'DESC')->get();

        $brand = Brand::all()->pluck('name', 'id')->toArray();  

        $countries = DB::table('countries')->orderBy('name', 'ASC')
                        ->pluck('name', 'id')->toArray();

        $measurement = DB::table('measurement')->orderBy('name', 'ASC')
                        ->pluck('name', 'id')->toArray();

        // variant
        $variants = DB::table('variants')
                        ->orderBy('name', 'ASC')
                        ->pluck('name', 'id')->toArray();

        // variant options
        $variant_options =  DB::table('variant_options')
                            ->orderBy('id', 'desc')
                            ->pluck("value", "id")->toArray();

        // attributes
        $attributes = DB::table('attributes')->orderBy('id', 'desc')
                        ->pluck("name", "id")->toArray();       
// echo "<pre>";
// print_r($productattributes);
// dd($attributes);

        return view('product.edit',compact( 'categories', 'brand', 'countries', 'measurement', 'variants', 'product', 'productattributes', 'variant_options', 'attributes', 'productvariants'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request, [
            'name' => 'required',
            'price' => 'required', 
            'description' => 'required',  
            'category_id' => 'required',        
            'image' => 'file|image|mimes:jpeg,png,jpg,gif,svg|max:2048'                       
        ]); 
        
        $input = $request->all();

        ####### Add Product ######
        $img = null;
        if ($request->hasfile('image')) {

            $file = $request->file('image');
            $response = $this->postImage($_FILES);

            if(isset($response['status']) && $response['status'] == 'SUCCESS'){
                $img = $response['data']['id'];
            }else{
                // error
            }        
        } 
        $product = Product::find($id);
       
        $in_prod = [
                'name'        => $input['name'],
                'description' => $input['description'],
                'price'       => $input['price'],
                'min_price'   => $input['min_price']?$input['min_price']: 0,
                'barcode'     => $input['barcode']?$input['barcode']: null,
                'measurement_id' => $input['measurement_id']?$input['measurement_id']: 0,
                'category_id' => $input['category_id'],
                'country_id'  => $input['country_id']?$input['country_id']: 0,
                'brand_id'    => $input['brand_id']? $input['brand_id']: 0,
                'default_image_id' => ($img >0) ? $img: $product->default_image_id,
                'is_blocked'  => $input['is_blocked'],
                ];

        $product->update($in_prod);

        ## Update IMAGE - PRODUCTID ##
        if($img > 0){
            DB::table('images')->where('id', $img)->update(['product_id' => $product->id]);
        }

        ####### Product Options ######
        if($product){  

            $in_prod_opt = [
               // 'sku'           => $input['sku']?$input['sku']: null,                
                'price'         => $input['price'],
                'count'         => $input['count'],
                'reserved_count'=> $input['reserved_count']?$input['reserved_count']: 0,           
                'variant_id'    => $input['variant_id']?$input['variant_id'] : null,
                'variant_option_id' => $input['variant_option_id']?$input['variant_option_id']: null,
                'updated_at'    => \Carbon\Carbon::now()  # new \Datetime()
            ];
            DB::table('product_options')->where('product_id',$product->id)->update($in_prod_opt);
        }

        ####### Product Variants ######
        if(isset($input['variant_id']) && $input['variant_id'] > 0){ 

            $in_prod_var = [               
                "variant_id"    => $input['variant_id'],
                "updated_at"    => \Carbon\Carbon::now()  # new \Datetime()
            ];
            DB::table('product_variants')->where('product_id', $product->id)->update($in_prod_var);
        }

        ####### Product Attributes ######
        if(isset($input['attribute_id']) && !empty($input['attribute_id'])){   
            foreach ($input['attribute_id'] as $key => $value) {
                $prod_opt = [
                        "attribute_id"  => $value,
                        "value"         => isset($input['attr_value'][$key])? $input['attr_value'][$key]: null,
                        "updated_at"    => \Carbon\Carbon::now()  # new \Datetime()
                    ];

                DB::table('product_attributes')
                    ->where('id', $key)
                    ->update($prod_opt);
            }
        }       

               
        return redirect()->route('products.index')
                        ->with('success','Products updated successfully');
    }


     public function deleteMultipleProducts(Request $request){

        if(!$request->ids){
            return response()->json(['status'=>false,'message' => "No products selected."]);  
        }

        $apids = explode(",", $request->ids);

        foreach ($apids as $key => $value) {
           $this->removeRow($value);
        }

        return response()->json(['status'=>true,'message'=>"Product deleted successfully."]);   

    }

    private function removeRow($id){

        $product = Product::find($id);

        DB::table('product_attributes')->where('product_id',$id)->delete();

        DB::table('product_options')->where('product_id',$id)->delete();

        DB::table('product_variants')->where('product_id',$id)->delete(); 

        DB::table('images')->where('product_id',$id)->delete();   


        $product->delete(); 

        return true;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $this->removeRow($id);

        return redirect()->route('products.index')
                        ->with('success','Product deleted successfully');

    }

    public function postImage($file){

        $ch = curl_init();
        
        $filePath = $file['image']['tmp_name'];
        $fileName = $file['image']['name'];
        $type = $file['image']['type'];
        $data = array('image' => curl_file_create($filePath, $type, $fileName));         
        
        curl_setopt($ch, CURLOPT_URL, 'https://api.menu4u.ae/api/image');
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,TRUE);
        $response = curl_exec($ch);    
        $err = curl_error($ch);
        curl_close($ch);

        if ($err) {
          return json_decode($err, true);
        } else {
          return json_decode($response, true);
        }
    }
}
