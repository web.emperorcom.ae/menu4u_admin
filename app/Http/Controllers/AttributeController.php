<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Attribute;
use App\Measurement;
use Validator,Response;
use DB;

class AttributeController extends Controller
{
    //
    //
	 /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $attribute = DB::table('attributes as at')
        ->leftJoin('measurement','measurement.id' , '=' , 'at.measurement_id')
        ->select('at.*', 'measurement.name as unit')
        ->orderBy('at.id', 'desc')
        ->get();

        return view('attributes.index',compact('attribute'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $units = Measurement::orderBy('name', 'asc')->pluck('name', 'id')->toArray();

        return view('attributes.create', compact('units'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'name' => 'required',
            'measurement_id' => 'required'
        ]);

        $input = $request->all();

        $lastId = Attribute::orderBy('id', 'desc')->first()->id;

        Attribute::create([ 
            'id'    => (int) $lastId + 1,
            'name'  => $input['name'],
            'default_value' => $input['default_value']?$input['default_value']: '',
            'options' => $input['default_value']?$input['default_value'] : '',
            'measurement_id' => $input['measurement_id']
            ]);
    
        return redirect()->route('attributes.create')
                        ->with('success','Attribute created successfully');
    }

    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $units = Measurement::orderBy('name', 'asc')->pluck('name', 'id')->toArray();
        $attribute = Attribute::find($id);

        return view('attributes.edit',compact( 'attribute', 'units'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $this->validate($request, [
            'name' => 'required',
            'measurement_id' => 'required'
        ]);

        $input = $request->all();
        $attribute = Attribute::find($id);

        $updata = [ 
            'name'  => $input['name'],
            'default_value' => $input['default_value']?$input['default_value']: '',
            'options' => $input['default_value']?$input['default_value'] : '',
            'measurement_id' => $input['measurement_id']
            ];

        $attribute->update($updata);        
               
        return redirect()->route('attributes.index')
                        ->with('success','Attribute updated successfully'); 
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //        
        $attr = Attribute::find($id); 

        $attr->delete(); 


        return redirect()->route('attributes.index')
                        ->with('success','Attribute deleted successfully');

    }
}
