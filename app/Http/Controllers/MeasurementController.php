<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Attribute;
use App\Measurement;
use Validator,Response;

class MeasurementController extends Controller
{
    //
	 /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $measurement = Measurement::orderBy('id','DESC')->get();

        return view('measurements.index',compact('measurement'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('measurements.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'name' => 'required',
            'short_name' => 'required'
        ]);

        $input = $request->all();

        $lastId = Measurement::orderBy('id', 'desc')->first()->id;

        Measurement::create([ 
            'id'    => (int) $lastId + 1,
            'name'  => $input['name'],
            'short_name' => $input['short_name'],
            'unit' => $input['unit'],
            'step' => 0.1
            ]);
    
        return redirect()->route('measurements.create')
                        ->with('success','Measurement created successfully');
    }

    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $measurement = Measurement::find($id);

        return view('measurements.edit',compact( 'measurement'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'short_name' => 'required'
        ]);
        $input = $request->all();
        $measurement = Measurement::find($id);

        $updata = [ 
            'name'  => $input['name'],
            'short_name' => $input['short_name'],
            'unit' => $input['unit'],
            'step' => 0.1
            ];

        $measurement->update($updata);        
               
        return redirect()->route('measurements.index')
                        ->with('success','Measurement updated successfully'); 
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //        
        $unit = Measurement::find($id); 
        // attribute 
        Attribute::where("measurement_id",$id)->delete();

        $unit->delete(); 


        return redirect()->route('measurements.index')
                        ->with('success','Measurement deleted successfully');

    }


    public function getAttributes(Request $request){
    	$data = [];
    	$data = Attribute::where("measurement_id",$request->measurement_id)
    				->get();

    	return response()->json($data);
    }


   
}
