<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use App\OrderStatus;
use App\ProductOrder;
use App\OrderChangelog;
use App\Product;

use Validator,Response;
use DB;
use Carbon\Carbon;
use Mail;
use Config;


class OrderController extends Controller
{   

    public $MAIL_FROM;
    public $MAIL_FROM_NAME;

    public $APP_URL;

    public function __construct(){

        $this->MAIL_FROM = Config::get('mail.from.address');
        $this->MAIL_FROM_NAME = Config::get('mail.from.name');
       
        $this->APP_URL = Config::get('app.url');

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        //$orders = Order::orderBy('created_at', 'DESC')->get();
        $query = DB::table('orders AS o')
                    ->join('order_status AS os', 'o.order_status_id', '=', 'os.id')
                    ->join('users AS u', 'o.user_id', '=', 'u.id')
                    ->select('o.id', 'o.comment', 'o.created_at','os.status', 'u.first_name', 'u.last_name')                   
                    ->orderBy('o.id', 'DESC');

        $orders = $query->get();
       

        return view('orders.index',compact('orders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $order  = DB::table('orders AS o')
                    ->join('order_status AS os', 'o.order_status_id', '=', 'os.id')
                    ->join('users AS u', 'o.user_id', '=', 'u.id')
                    ->select('o.id', 'o.user_id', 'o.order_status_id', 'o.comment', 'o.created_at','os.status', 'u.first_name', 'u.last_name')
                    ->where('o.id',  '=', $id)
                    ->first();

        $address = DB::table('addresses AS a')
			        ->join('users as u', 'a.user_id', '=', 'u.id')
			        ->join('countries as c', 'a.country_id', '=', 'c.id')
			        ->select('a.*', 'c.name as country')
			        ->where('u.id','=', $order->user_id)
			        ->first();

        $phones = DB::table('phones AS p')
			        ->join('users as u', 'p.user_id', '=', 'u.id')
			        ->join('countries as c', 'p.country_id', '=', 'c.id')
			        ->select('p.*', 'c.name as country')
			        ->where('u.id','=', $order->user_id)
			        ->first();

		$emails = DB::table('emails AS e')
			        ->join('users as u', 'e.user_id', '=', 'u.id')
			        ->select('e.*')
			        ->where('u.id','=', $order->user_id)
			        ->first();

        $history = DB::table('order_changelog as oc')
        			->join('orders AS o', 'oc.order_id', '=', 'o.id')
                    ->join('order_status AS os', 'o.order_status_id', '=', 'os.id')
                    ->select('os.status as status_name', 'oc.*')         
                    ->where('oc.order_id', '=', $id)
                    ->orderBy('oc.created_at', 'DESC')
                    ->get();

        $products = DB::table('product_orders as po')
                    ->join('products as p', 'po.product_option_id', '=', 'p.id')
                    ->select('po.quantity', 'p.name', 'p.price', 'p.min_price')         
                    ->where('po.order_id', '=', $id)
                    ->orderBy('po.created_at', 'DESC')
                    ->get();
   
        $status_lists = OrderStatus::orderBy('id', 'ASC')->pluck('status', 'id')->toArray();

        return view('orders.show',compact('order', 'history', 'products', 'status_lists', 'address', 'phones', 'emails'));
    }

    public function printPreview($id)
    {
        //
        $order  = DB::table('orders AS o')
                    ->join('order_status AS os', 'o.order_status_id', '=', 'os.id')
                    ->join('users AS u', 'o.user_id', '=', 'u.id')
                    ->select('o.id ', 'o.user_id', 'o.order_status_id', 'o.comment', 'o.created_at','os.status', 'u.first_name', 'u.last_name')
                    ->where('o.id',  '=', $id)
                    ->first();

        $address = DB::table('addresses AS a')
			        ->join('users as u', 'a.user_id', '=', 'u.id')
			        ->join('countries as c', 'a.country_id', '=', 'c.id')
			        ->select('a.*', 'c.name as country')
			        ->where('u.id','=', $order->user_id)
			        ->first();

        $phones = DB::table('phones AS p')
			        ->join('users as u', 'p.user_id', '=', 'u.id')
			        ->join('countries as c', 'p.country_id', '=', 'c.id')
			        ->select('p.*', 'c.name as country')
			        ->where('u.id','=', $order->user_id)
			        ->first();

		$emails = DB::table('emails AS e')
			        ->join('users as u', 'e.user_id', '=', 'u.id')
			        ->select('e.*')
			        ->where('u.id','=', $order->user_id)
			        ->first();    

        $products = DB::table('product_orders as po')
                    ->join('products as p', 'po.product_option_id', '=', 'p.id')
                    ->select('po.quantity', 'p.name', 'p.price', 'p.min_price')         
                    ->where('po.order_id', '=', $id)
                    ->orderBy('po.created_at', 'DESC')
                    ->get();

        return view('orders.printpreview',compact('order', 'products', 'address', 'phones', 'emails'));
    }

     // Change the order status
    public function changeStatus(Request $request){
        
        $this->validate($request, [      
            'order_id'   => 'required',
            'order_status_id' => 'required'     
        ]); 

        $input = $request->all();

        $log = OrderChangelog::orderBy('id', 'desc')->first();
        $last = $log? $log->id: 0;
        // Insert history
        $insert = [
        		"id"	   => $last + 1,
        		"date"	   => \Carbon\Carbon::now(),
        		"status"   => $input['comments'],
                "order_id" => $input['order_id'],
                "user_id"  => auth()->user()->id
                ];
        
        OrderChangelog::create($insert);
     
        ## Update Order
        $order = Order::find($input['order_id']);

        $update = [];
        $update['order_status_id'] =  $input['order_status_id'];      
        $update['updated_at']      =  \Carbon\Carbon::now();
        $order->update($update);

        return redirect()->route('orders.show', $order->id)
                        ->with('success','Order status updated successfully');

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        // History Delete
        OrderChangelog::where('order_id', '=', $id)->delete();
        
        // Items Delete
        ProductOrder::where('order_id', '=', $id)->delete();

        // Order delete
        Order::find($id)->delete(); 

        return redirect()->route('orders.index')
                        ->with('success','Order deleted successfully');
    }

 
}