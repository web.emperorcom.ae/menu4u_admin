<?php
   
namespace App\Http\Controllers;
  
use Illuminate\Http\Request;
use App\User;
use App\Order;
use Illuminate\Support\Facades\Auth;
use DB;
   
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
  
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $customers = User::where('role_id', 1)->count();

        $completed = Order::where('order_status_id', 4)->count();
        $rejected  = Order::whereIn('order_status_id', [6,7])->count();
        $allorders = Order::count();

        $query = DB::table('orders AS o')
                    ->join('order_status AS os', 'o.order_status_id', '=', 'os.id')
                    ->join('users AS u', 'o.user_id', '=', 'u.id')
                    ->select('o.id', 'o.comment', 'o.created_at','os.status', 'u.first_name', 'u.last_name')                   
                    ->orderBy('o.id', 'DESC');

        $latest = $query->take(5)->get();
       

        return view('home', compact('customers','completed', 'rejected', 'allorders', 'latest'));
    } 
}