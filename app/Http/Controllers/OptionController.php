<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Option;
use App\Product;

class OptionController extends Controller
{
    //
    public function index()
    {
    	$options = Option::all();

    	return view('options.index',compact('options'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $option = Option::find($id);

        $products = Product::all()->pluck('name', 'id')->toArray();

        $data = $this->viewData($option->value);
  
        $cities = ['Abu Dhabi', 'Dubai', 'Sharjah', 'Ajman', 'Umm Al Quwain', 'Fujairah', 'Ras Al Khaimah'];
        
        return view('options.edit',compact('option', 'data', 'products', 'cities'));
    }

    public function viewData($content){
    	$data =  json_decode($content, true);

    	return isset($data['data'])? $data['data'] : $data;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {       
        $this->validate($request, [
            'key' => 'required',
            'image_path' => 'file|image|mimes:jpeg,png,jpg,gif,svg|max:2048'    
        ]);
       
        $input = $request->all();       
        $option = Option::find($id);
       
        $updata = [];

        switch($option->key){
        	case 'home_carousel':
        		// new
        		$hbanner = [];
	        	$hbanner['image'] = $this->uploadBanners($request);
	        	$hbanner['productId'] = (int) $input['productId'];
	        	// existing
	        	$updata = $this->viewData($option->value);
	       		// merge new and existing
	        	array_push($updata, $hbanner);
	        	break;
	        case 'privacy':
        		// new   
        		$static = [];     		
	        	$static['title'] = $request->title;
	        	$static['content'] = $request->description;

	        	array_push($updata, $static);
	        	break;
	        case 'terms':
        		// new   
        		$static = [];     		
	        	$static['title'] = $request->title;
	        	$static['content'] = $request->description;

	        	array_push($updata, $static);
	        	break;
        }
        if(!empty($updata)){
        	$option->value = json_encode(['data' => $updata]);       
        	$option->save();
        }
               
        return redirect()->route('options.edit', $id)
                        ->with('success','Option updated successfully');
    }

    public function uploadBanners($request){

    	$img = null;
        if ($request->hasfile('image_path')) {

            $file = $request->file('image_path');
            $response = $this->postImage($_FILES);

            if(isset($response['status']) && $response['status'] == 'SUCCESS'){
                $img = $response['data']['url'];
            }else{
                // error
            }        
        } 
        return $img;

    }

    public function deleteBanners(Request $request)
    {

        $id = $request->optid;
        $indx = $request->id;

        $option = Option::find($id);
		$banners = $this->viewData($option->value);

		if(isset($banners[$indx])){
			// delete banner image
	        unset($banners[$indx]);
	        
	        $option->value = json_encode(['data' => $banners]);       
        	$option->save();

	        return response()->json(['status'=>true,'message' => "Image deleted successfully."]);  
		
		}else{
           
            return response()->json(['status'=>false,'message' => "There is an error to delete this image."]);   
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {    
        //
        Option::find($id)->delete();

        return redirect()->route('options.index')
                        ->with('success','Option deleted successfully');
    }

    public function postImage($file){

        $ch = curl_init();
        
        $filePath = $file['image_path']['tmp_name'];
        $fileName = $file['image_path']['name'];
        $type = $file['image_path']['type'];
        $data = array('image' => curl_file_create($filePath, $type, $fileName));         
        
        curl_setopt($ch, CURLOPT_URL, 'https://api.menu4u.ae/api/image');
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,TRUE);
        $response = curl_exec($ch);    
        $err = curl_error($ch);
        curl_close($ch);

        if ($err) {
          return json_decode($err, true);
        } else {
          return json_decode($response, true);
        }
    }
}
