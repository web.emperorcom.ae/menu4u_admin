<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Contracts\Auth\Authenticatable;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request)
    {  
       
        $inputVal = $request->all();
   
        $this->validate($request, [
            'username' => 'required',
            'password' => 'required',
        ]);

        $user = $this->validAuth($inputVal);

        if(!$user){
            return redirect()->back()->withErrors(['Invalid username or password.']);

        }else{
            if(Auth::loginUsingId($user->id)){ 
                return redirect()->route('home');
            }
        }
        
    }

    protected function validAuth($data){
        return User::where('username', 'LIKE', $data['username'])
                ->where('password', 'LIKE', sha1($data['password']))
                ->where('role_id', 2)// is admin user
                ->first();
    }
   
}