<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\User;
use Validator,Response;
use DB;
use Carbon\Carbon;
use Mail;
use Config;
//use App\Jobs\SendEmailJob;

class PassportAuthController extends Controller
{   
    public $MAIL_FROM;
    public $MAIL_FROM_NAME;

    public $WEB_URL;

    public function __construct(){

        $this->MAIL_FROM = Config::get('mail.from.address');
        $this->MAIL_FROM_NAME = Config::get('mail.from.name');

        $this->WEB_URL = Config::get('app.web_url');       
    }
    /**
     * Registration Req
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'first_name' => 'required|string|max:255',
            'last_name'  => 'required|string|max:255',
            'username'   => 'required|string|max:255|unique:users',
            'password'   => 'required|string|min:6',
            'email'      => 'required|string|max:255|email',
            'phone'      => 'required|string|min:10'
        ]);
        if ($validator->fails())
        {
            return response()->json(['status'=> false,'errors'=>$validator->errors()->all()], 422);           
        }

        $lastid = User::orderBy('id', 'desc')->first();

        $data = ['id'         => $lastid + 1,
                'first_name'  => $request->first_name,
                'last_name'   => $request->last_name,
                'username'    => $request->username,               
                'password'    => sha1($request->password),
                'role_id'     => 1
                ];

        // Insert User
        $user = User::create($data);
        $userid = $user->id;

        // Insert email
        if(isset($request->email) && $request->email){
            $email_last = DB::table('emails')->orderBy('id', 'desc')->first();
            $email_last_id = $email_last->id;
            $code = (string) Str::uuid();

            DB::table('emails')->insert([
                    'id'        => $email_last_id 
                    'email'     => $request->email,
                    'approved'  => false,
                    'code'      => $code,
                    'user_id'   => $userid
                ]);  
        }

        // Insert phone
        if(isset($request->phone) && $request->phone){
            $phone_last = DB::table('phones')->orderBy('id', 'desc')->first();
            $phone_last_id = $phone_last->id;
            DB::table('phones')->insert([
                    'id'        => $email_last_id 
                    'phone'     => $request->phone,
                    'approved'  => false,
                    'user_id'   => $userid,
                    'country_id'=> 234 // UAE
                ]);  
        }
        
        // $data = ['name' => $request->name,  'email' => $request->email, 'password' => $request->password];
        // $email = $request->email;

        // Mail::send('emails.welcomemail', $data, function($message) use ($email) {
        //     $message->to($email)->subject('Welcome to Mammas Pizza');

        //     $message->from($this->MAIL_FROM, $this->MAIL_FROM_NAME);
        // });
        //$data['job_type'] = 'signup';

        // send registration email
        //dispatch(new SendEmailJob($data));


        return response()->json(['status'=> true, 'user' => $user], 200);
    }

    // If email already exists
    public function emailAlreadyExists($email)
    {
        try{
            if (User::where('email', '=', $email)->exists()) {
               // user found
                return response()->json(['status' => true, 'count' => 1], 200);
            }       

        }catch(\Exception $exception){

            $response = ['status'=> false,'errors' => $exception->getMessage()];
            return response()->json($response, 422);
        }
    }
 
    /**
     * Login Req
     */
    public function login(Request $request)
    { 
        
        $validator = Validator::make($request->all(), [
            'username' => 'required',
            'password' => 'required|min:6',
        ]);
        if ($validator->fails())
        {
            return response()->json(['status'=> false,'errors'=>$validator->errors()->all()], 422); 
        }

        $user = User::where('email', $request->email)->first();
        if ($user) {
            $data = [
                'email' => $request->email,
                'password' => $request->password
            ];
 
            if (auth()->attempt($data)) {
                $token = auth()->user()->createToken('LaravelAuthApp')->accessToken;
            
                return response()->json(['status'=> true,'token' => $token, 'user' => $user], 200);
            } else {
                $response = ['status'=> false,'errors' => "Password mismatch"];
                return response()->json($response, 422);
            }
        } else {
            $response = ['status'=> false,'errors' => 'User does not exist'];
            return response()->json($response, 422);
        }

    }

     
    // Forgot password
    public function forgot(Request $request)
    { 
        
        $validator = Validator::make($request->all(), [
            'email' => 'required|email'
        ]);
        if ($validator->fails())
        {
            return response()->json(['status'=> false,'errors'=>$validator->errors()->all()], 422); 
        }
        $email = $request->email;

        $user = User::where('email', $email)->first();    
        if ($user) {
            $token = Str::random(10);

            try{
                DB::table('password_resets')
                    ->insert([
                        'email' => $email, 
                        'token' => $token,
                        'created_at' => \Carbon\Carbon::now()
                    ]);
                
                $data = ['name' => $user->name,
                        'link'  => $this->WEB_URL.'/resetpassword?token='.$token
                        ];
                // Send mail
                Mail::send('emails.forgot', $data, function($message) use ($email) {
                    $message->to($email)->subject('Reset Your Password - Mammas Pizza');

                    $message->from($this->MAIL_FROM, $this->MAIL_FROM_NAME);
                });

                return response()->json(['status'=> true,'message' => 'Please check your inbox'], 200);

            }catch(\Exception $exception){

                $response = ['status'=> false,'errors' => $exception->getMessage()];
                return response()->json($response, 422);
            }
        
        } else {
            $response = ['status'=> false,'errors' => 'User does not exist'];
            return response()->json($response, 422);
        }

    }
     // Reset password
    public function resetPassword(Request $request)
    { 
        
        $validator = Validator::make($request->all(), [
            'password' => 'required|string|min:6',
            'token'    => 'required'           
        ]);
        if ($validator->fails())
        {
            return response()->json(['status'=> false,'errors'=>$validator->errors()->all()], 422); 
        }

        $pass  = DB::table('password_resets')
                    ->where('token','=',$request->token)
                    ->orderBy('created_at', 'DESC')->first();

        if ($pass) {
            
            try{ // update user password
                $user = User::where('email','=', $pass->email)->first();
                $user->update(['password' => bcrypt($request->password)]);
               
                $data = [
                    'email' => $user->email,
                    'password' => $request->password
                ];
 
                if (auth()->attempt($data)) {
                    $newtoken = auth()->user()->createToken('LaravelAuthApp')->accessToken;
                
                    return response()->json(['status'=> true,'token' => $newtoken, 'user' => $user], 200);
                } 
            }catch(\Exception $exception){

                $response = ['status'=> false,'errors' => $exception->getMessage()];
                return response()->json($response, 422);
            }
        
        } else {
            $response = ['status'=> false,'errors' => 'Token does not exist'];
            return response()->json($response, 422);
        }

    }    
   
    public function logout (Request $request) 
    {
        $token = $request->user()->token();
        if(!$token){
            $response = ['status'=> false,'errors' => "Token Invalid"];
            return response()->json($response, 422);
        }
        
        $token->revoke();
        $response = ['status' => true, 'message' => 'You have been successfully logged out!'];
        return response()->json($response, 200);
    }

    /**
     * Returns Authenticated User Details
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function userDetails(Request $request)
    {
        dd($request);
        if(!$request->user()->token()){
            $response = ['status'=> false,'errors' => "Token Invalid"];
            return response()->json($response, 422);
        }

        $response = ['status'=> true,'token' => $request->user()->token(), 'user' => auth()->user()];
        return response()->json($response, 200);
    }

}