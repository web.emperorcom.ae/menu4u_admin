<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Brand;

class BrandController extends Controller
{
    //
    public function index()
    {
    	$brand = Brand::orderBy('name','ASC')->get();

    	return response()->json(['success' => true,
                                'data'     => $brand],
                            200);
    }

}
