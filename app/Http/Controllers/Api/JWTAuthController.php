<?php

namespace App\Http\Controllers\Api;

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Validator,Response;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Str;
use DB;
use Carbon\Carbon;
use Mail;
use Config;
use JWTAuth;

class JWTAuthController extends Controller
{	

	public $MAIL_FROM;
    public $MAIL_FROM_NAME;
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
       // $this->middleware('auth:api', ['except' => ['login', 'register']]);

        $this->MAIL_FROM = Config::get('mail.from.address');
        $this->MAIL_FROM_NAME = Config::get('mail.from.name');
    }

    /**
     * Register a User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [         
            'fullname' => 'required|string|between:2,100',
            'username'   => 'required|string|between:4,15|unique:users',
            'password'   => 'required|confirmed|string|min:6',
            'email'      => 'required|email|max:50',
            'phone'      => 'required|string|between:10,15'
        ]);

        if ($validator->fails())
        {
            return response()->json(['status'=> false,'errors'=>$validator->errors()->all()], 422);
        }
        if(isset($request->email) && $request->email){
        	$isemail = DB::table('emails')->where('email','=', $request->email)->count();

        	if($isemail > 0){
        		return response()->json(['status'=> false,'errors'=> ['Email already exists']], 422);
        	}
        }
        if(isset($request->phone) && $request->phone){
        	$isphone = DB::table('phones')->where('number','=', $request->phone)->count();

        	if($isphone > 0){
        		return response()->json(['status'=> false,'errors'=> ['Phone number already exists']], 422);
        	}
        }

        $user_last = User::orderBy('id', 'desc')->first();
        $lastid = $user_last->id;
        $fname = explode(" ", $request->fullname);

        $data = ['id'         => (int) $lastid + 1,
                'first_name'  => isset($fname[0])? $fname[0]: $fname,
                'last_name'   => isset($fname[1])? $fname[1]: null,
                'username'    => $request->username,         
                'password'    => sha1($request->password),
                'birthday'	  => null,
                'is_blocked'  => false,
                'facebook_id' => null,
                'firebase_id' => null,
                'google_id'   => null,
                'default_address_id' => null,
                'default_email_id' => null,
                'default_phone_id' => null,
                'role_id'     => 1
                ];

        $userid = DB::table('users')->insertGetId($data);

        // Insert email
        if(isset($request->email) && $request->email){
            $email_last = DB::table('emails')->orderBy('id', 'desc')->first();            
            $email_last_id = $email_last->id;

            $code = (string) Str::uuid();

            DB::table('emails')->insert([
                    'id'        => (int) $email_last_id + 1,
                    'email'     => $request->email,
                    'approved'  => false,
                    'code'      => $code,
                    'user_id'   => $userid
                ]);  
       }

        // Insert phone
        if(isset($request->phone) && $request->phone){
            $phone_last = DB::table('phones')->orderBy('id', 'desc')->first();
            $phone_last_id = $phone_last->id;

            DB::table('phones')->insert([
                    'id'        => (int) $phone_last_id + 1, 
                    'number'     => $request->phone,
                    'approved'  => false,
                    'user_id'   => $userid,
                    'country_id'=> 234 // UAE
                ]);  
        }
        // result        
        $result = $this->userResult($userid);

        return response()->json($result, 201);
    }

    public function userResult($userid){

        // result
        $user = User::find($userid);
        $user_email = DB::table('emails')->where('user_id', '=', $user->id)->first();
        $user->email = $user_email->email;

        $user_phones = DB::table('phones')->where('user_id', '=', $user->id)->first();
        $user->phone = $user_phones->number;

        $user_address = DB::table('addresses')->where('user_id', '=', $user->id)->first();
        $user->address = ($user_address)?$user_address : [];

        $token = JWTAuth::fromUser($user);

        $result =  $this->createNewToken($token);
        $result['user'] = $user;
        $result['status'] = true;

        return $result;
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
    	$validator = Validator::make($request->all(), [
            'username' => 'required',
            'password' => 'required|string|min:6',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $user = User::where('username', $request->username)->where('role_id', 1)->first();
        if ($user) {
            $data = [
                'username' => $request->username,
                'password' => $request->password
            	];
            $user = $this->validAuth($data);

            if(!$user){
                $response = ['status'=> false,'errors' => "Password mismatch"];
                return response()->json($response, 422);
            }else{
            	
				// result        
                $result = $this->userResult($user->id);

        		return response()->json($result, 201);
            }
        } else {
            $response = ['status'=> false,'errors' => 'User does not exist'];
            return response()->json($response, 422);
        }
       
    }

    protected function validAuth($data){
        return User::where('username', 'LIKE', $data['username'])
        		->where('password', 'LIKE', sha1($data['password']))
                ->where('role_id', 1)// is user
                ->first();
    }
    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function profile(Request $request)
    {	
    	
        // if(!$request->user()->token()){
        //     $response = ['status'=> false,'errors' => "Token Invalid"];
        //     return response()->json($response, 422);
        // }

        $user = auth('api')->user();
        
        $result = $this->userResult($user->id);

        return response()->json($result, 200);
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout(Request $request)
    {
        //auth('api')->logout();
    	//$token = $request->user()->token();
        // if(!$token){
        //     $response = ['status'=> false,'errors' => "Token Invalid"];
        //     return response()->json($response, 422);
        // }
        //$token->revoke();
        auth('api')->logout();

        return response()->json(['status' => true, 'message' => 'Successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->createNewToken(auth('api')->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function createNewToken($token)
    {	
    	return [
            'token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth('api')->factory()->getTTL() * 60
        ];

        // return response()->json([
        //     'access_token' => $token,
        //     'token_type' => 'bearer',
        //     'expires_in' => auth('api')->factory()->getTTL() * 60
        // ]);
    }
}
