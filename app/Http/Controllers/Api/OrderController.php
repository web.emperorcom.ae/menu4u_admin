<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Order;
use App\ProductOrder;
use App\OrderChangelog;
use App\User;
use Validator,Response;
use DB;
use Carbon\Carbon;
use Mail;
use Config;
use App\Product;
//use App\Events\NewOrders;
//use App\Jobs\SendEmailJob;

class OrderController extends Controller
{
    //
    public $MAIL_FROM;
    public $MAIL_FROM_NAME;

    public $WEB_URL;
    public $APP_URL;

    public $site_settings = [];

    public function __construct(){

        $this->MAIL_FROM = Config::get('mail.from.address');
        $this->MAIL_FROM_NAME = Config::get('mail.from.name');

        $this->APP_URL = Config::get('app.url');
        $this->WEB_URL = Config::get('app.web_url');

        $this->site_settings = ['tax_amount' => 5, 'min_limit' => 100];
    }

    protected function getLastID(){
        $lastRecord = DB::table('orders')->orderBy('id', 'DESC')->first();
        $inv_no = $lastRecord ? $lastRecord->id : 0;

        return (int) $inv_no;
    }

    protected function priceCalculate($products){

        if(!empty($products)){
            $sub_total = 0;

            foreach ($products as $key => $value) {

                $prod_data  = Product::find($value['id']);
                $price = $prod_data->price;
                $total = $price * $value['count'];
                // PRODUCTS
                $products[$key]['price'] = $price;
                $products[$key]['total'] = $total;
                $sub_total += $total;
            }

            return ['sub_total' => $sub_total, 'products' => $products];
        }
    }  

    // public function createUser($request){
    //     // register user details 
    //     $user_data =  ['fullname'   => implode(' ',[$request->firstname, $request->lastname]),
    //                    'email'  => $request->email,
    //                    'phone'  => $request->phone,
    //                    'username' => $request->username,
    //                    'password' => $request->password,
    //                    'password_confirmation' => $request->password
    //                 ];


    //     $ures =  app(JWTAuthController::class)->register($user_data);
    //     $userRes = json_decode($ures);

    //     return $userRes;
    // }  
    public function createUser($request){

        $validator = Validator::make($request->all(), [     
            'username'   => 'required|string|between:4,15|unique:users',
            'password'   => 'required|confirmed|string|min:6'
        ]);

        if ($validator->fails())
        {
            return ['status'=> false,'errors'=>$validator->errors()->all()];
        }
        if(isset($request->email) && $request->email){
            $isemail = DB::table('emails')->where('email','=', $request->email)->count();

            if($isemail > 0){
                return ['status'=> false,'errors'=> ['Email already exists']];
            }
        }
        if(isset($request->phone) && $request->phone){
            $isphone = DB::table('phones')->where('number','=', $request->phone)->count();

            if($isphone > 0){
                return ['status'=> false,'errors'=> ['Phone number already exists']];
            }
        }

        $user_last = User::orderBy('id', 'desc')->first();
        $lastid = $user_last->id;

        $data = ['id'         => (int) $lastid + 1,
                'first_name'  => $request->firstname,
                'last_name'   => $request->lastname,
                'username'    => $request->username,         
                'password'    => sha1($request->password),
                'birthday'    => null,
                'is_blocked'  => false,
                'facebook_id' => null,
                'firebase_id' => null,
                'google_id'   => null,
                'default_address_id' => null,
                'default_email_id' => null,
                'default_phone_id' => null,
                'role_id'     => 1
                ];

        $userid = DB::table('users')->insertGetId($data);

        // Insert email
        if(isset($request->email) && $request->email){
            $email_last = DB::table('emails')->orderBy('id', 'desc')->first();            
            $email_last_id = $email_last->id;

            $code = (string) Str::uuid();

            DB::table('emails')->insert([
                    'id'        => (int) $email_last_id + 1,
                    'email'     => $request->email,
                    'approved'  => false,
                    'code'      => $code,
                    'user_id'   => $userid
                ]);  
       }

        // Insert phone
        if(isset($request->phone) && $request->phone){
            $phone_last = DB::table('phones')->orderBy('id', 'desc')->first();
            $phone_last_id = $phone_last->id;

            DB::table('phones')->insert([
                    'id'        => (int) $phone_last_id + 1, 
                    'number'     => $request->phone,
                    'approved'  => false,
                    'user_id'   => $userid,
                    'country_id'=> 234 // UAE
                ]);  
        }

        $user = User::find($userid);
        $user_email = DB::table('emails')->where('user_id', '=', $user->id)->first();
        $user->email = $user_email->email;

        $user_phones = DB::table('phones')->where('user_id', '=', $user->id)->first();
        $user->phone = $user_phones->number;

        return ['status' => true, 'data' => $user];
    }

    public function emailID($request, $userid){
        return DB::table('emails')
                ->where('user_id', '=', $userid)
                ->where('email','=', $request->email)
                ->first();
    }
    
    public function phoneID($request, $userid){
        return DB::table('phones')
                ->where('user_id', '=', $userid)
                ->where('number','=', $request->phone)
                ->first();
    }

    public function insertAddress($request, $userid){
        $addressid = 0;

        if(!$request->address_id){

            $last_row = DB::table('addresses')->orderBy('id','desc')->first();
            $lastid = $last_row->id;

            $data = [
                'id'        => $lastid + 1,
                'zip'       => null,
                'region'    => $request->area,
                'city'      => $request->city,
                'line1'     => $request->address,
                'line2'     => null,
                'user_id'   => $userid,
                'created_at'   => \Carbon\Carbon::now(),
                'updated_at'   => \Carbon\Carbon::now(),
                'country_id'   => 234
            ];
            $addressid = DB::table('addresses')->insertGetId($data);

            return $addressid;
        }
    }

    public function createOrder(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'firstname' => 'required|string',
            'lastname'  => 'required|string',
            'email'     => 'required|email',
            'phone'     => 'required|min:10',
            'address'   => 'required'
        ]);
       
        if ($validator->fails())
        {
            return response()->json(['status'=> false, 'errors' => $validator->errors()->all()], 422);
        }
        $min_limit = $this->site_settings['min_limit'];
        if($request->total_amount < $min_limit){
            return response()->json(['status'=> false, 'errors' => 'You must order amount above '.$min_limit.' AED'], 422);
        }
        //Price Calcuation
        $prodz = json_decode($request->products, true);

        if(empty($prodz)){
            return response()->json(['status'=> false, 'errors' => 'Products are empty'], 422);   
        }

        ## USER
        $userid = 0;
        if(!$request->user_id){
            // user create
            $user_info = $this->createUser($request);

            if(isset($user_info['status']) && !$user_info['status']){
                return response()->json($user_info, 422);
            }else{
               // return response()->json($user_info, 200);
                $userid = $user_info['data']->id;
            }           
        }else{
            $userid = $request->user_id? $request->user_id : 0;
        }


        ## email
        $emailid = 0;
        $email_row = $this->emailID($request, $userid);
        if(!$email_row){
            // email create
        }else{
            $emailid = $email_row->id;
        }

        ## phone
        $phoneid = 0;
        $phone_row = $this->phoneID($request, $userid);
        if(!$phone_row){
            // email create
        }else{
            $phoneid = $phone_row->id;
        }
        
        ## ADDRESS
        $addressid = 0;
        if(!$request->address_id){
            // user address create 
            $addressid = $this->insertAddress($request, $userid);
        }else{
            $addressid = $request->address_id? $request->address_id : 0;
        }  


        ## Products and its price 
        $cal_res    = $this->priceCalculate($prodz);

        $products   = $cal_res['products'];
        $sub_total  = $cal_res['sub_total'];
        $vat_amt    = 0; 
        $total_amount = 0;
        $delivery   = $request->delivery;// 5AED for delivery

        ## VAT 
        $vat_amt = $sub_total * ($this->site_settings['tax_amount']/100);
        $total_amount = $vat_amt + $sub_total + $delivery;
      
        // CREATE ORDER       
        $last_ord_id = $this->getLastID();       

        $oInsert = [
            'id'            => $last_ord_id + 1,
            'comment'       => $request->comments,
            'order_status_id' => 1,   
            'address_id'    => $addressid,
            'phone_id'      => $phoneid,
            'email_id'      => $emailid,
            'user_id'       => $userid,
            'created_by_user_id' => $userid,
            'updated_by_user_id' => $userid                
            
        ]; 
        $order = Order::create($oInsert);

        // CREATE ORDER HISTORIES
        $history = OrderChangelog::orderBy('id', 'desc')->first();  
        $his_last = ($history)? (int) $history->id: 0;     
        $hisInsert = [
            'id'        => $his_last + 1,
            'date'      => \Carbon\Carbon::now(),
            'status'    => "Order is pending",
            'order_id'  => $order->id,
            'user_id'   => $userid
        ]; 
        $orderHis = OrderChangelog::create($hisInsert);        

        // CREATE ORDER ITEMS - Loop
        $itmInsert = [];    
        $orderitem = ProductOrder::orderBy('id', 'desc')->first(); 
        $item_last = ($orderitem)? $orderitem->id: 0;

        if(!empty($products)){ 
            $ID = $item_last;
            foreach ($products as $key => $value) {
                $ID = $ID +1;

                $itmInsert = [
                    'id'           => $ID,                                        
                    'quantity'     => $value['count'],
                    'order_id'     => $order->id,
                    'product_option_id'   => $value['id']? $value['id']: 0,
                ]; 
                $orderItem = ProductOrder::create($itmInsert);

            }
        }
        $data = ['order' => $order];

        // Send Notification        
       // $this->sendOrderNotification($order); 

        // SEND EMAIL - ORDER SUMMARY
      //  $mail =  $this->emailOrderSummary($order);   

        return response()->json(['status' => true,
                                'data' => $data,
                                'message' => 'Order placed successfully'], 200);
        
    }

  
    // send notification to admin
    protected function sendOrderNotification($order){

        $loc = ($order->delivery_method == 'pickup')? $order->address: $order->area;

        /****************************************************/
        $notify =  array(
                    "title" => "New Order # ".$order->invoiceno." on ".$location->name
                  );

        event(new NewOrders($notify));       
    }

    // Send email for order summary
    protected function emailOrderSummary($order){

        $data = [
            'invoiceno'     => $order->invoiceno,            
            'fullname'      => $order->fullname,
            'email'         => $order->email,
            'phone'         => $order->phone,
            'address'       => $order->address,
            'city'          => $order->city? $order->city : '',
            'area'          => $order->area? $order->area: '',
            'sub_total'     => $order->sub_total,
            'delivery_cost' => $order->delivery_cost,
            'vat_price'     => $order->vat_price,
            'discount'      => $order->discount,
            'total_amount'  => $order->total_amount,
            'delivery_method' => $order->delivery_method,
            'payment_method'  => $order->payment_method,
            'created_at'      => $order->created_at
        ]; 
        $tomail = $data['email'];
        $invoice = $data['invoiceno'];
        
        // Order Products
        $products = OrderProduct::where('order_id', '=', $order->id)->get();
                     
        $data['products'] = $products;   

        $data['job_type'] = 'order';
        // Order Confirmation mail        
        dispatch(new SendEmailJob($data));      

        return true;
    }
}
