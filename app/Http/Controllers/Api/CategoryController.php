<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Category;
use DB;

class CategoryController extends Controller
{
    //
    public function index(Request $request)
    {
        // 
       	$cat = DB::table('categories as c')
	            ->select('c.*')  
	            ->where('c.category_id', '=', 2) // Super Market         
	            ->orderBy('c.name', 'DESC')
	            ->get();
    	
    	$category = []; $subcat = [];
		foreach ($cat as $key => $value) {
			$category[$key] = $value;
			$category[$key]->subcategory = DB::table('categories as c')
									            ->select('c.*')  
									            ->where('c.category_id', '=', $value->id)         
									            ->orderBy('c.name', 'DESC')
									            ->get();

		}
    	return response()->json(['success' => true,
                                'data'     => $category],
                            200);
    }
}
