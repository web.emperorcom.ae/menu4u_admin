<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Option;

class BannerController extends Controller
{
    //
    public function index()
    {

    	$banner = Option::where('key','=','home_carousel')->first();
    	
    	return response()->json(['success' => true,
                                'data'     => json_decode($banner->value)],
                            200);
    }
}
