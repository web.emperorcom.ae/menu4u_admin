<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Product;
use DB;

class ProductController extends Controller
{
    //
    public function newProducts(){
        $products = DB::table('products as p')                    
                    ->leftJoin('images as im', 'p.default_image_id', '=', 'im.id')
                    ->select('p.*', 'im.url as image')
                    ->orderBy('id', 'DESC')
                    ->where('p.is_blocked', '!=', true)
                    ->offset(0)->limit(12)->get();

        return response()->json(['success' => true,
                                'data'     => $products],
                            200);
    }

    public function products(Request $request){
        $input = $request->all();

    	$sql = DB::table('products as p')
                ->leftJoin('images as im', 'p.default_image_id', '=', 'im.id')
                ->select('p.*', 'im.url as image')
                ->where('p.is_blocked', '!=', true);

        if(!empty($input)){

            if(isset($input['c']) && trim($input['c']) !=''){
                $cterm = (int) trim($input['c']);

                $cats = DB::table('categories')
                            ->where('category_id', '=', $cterm)
                            ->pluck('id')->toArray();
 
                if(!empty($cats)){
                    array_push($cats, $cterm);

                    $sql->join('categories as c', 'p.category_id', '=', 'c.id')
                        ->whereIn('c.id', $cats);
                }else{
                    $sql->join('categories as c', 'p.category_id', '=', 'c.id')
                        ->where('c.id','=', $cterm);
                }
                

            }
            if(isset($input['b']) && trim($input['b']) !=''){ 
                $bterm = array_map('intval', explode(',', trim($input['b']))); 
                
                $sql->whereIn('p.brand_id',$bterm);
            }

            if(isset($input['s']) && trim($input['s']) !=''){ 
                //$terms = explode(" ", trim($input['s']));
                $terms = trim($input['s']);

                $sql->where('p.name', 'ilike', "%{$terms}%") 
                    ->orWhere('p.description', 'ilike', "%{$terms}%");
            }
            
            if(isset($input['p']) && trim($input['p']) !=''){ 
                $pterm = array_map('intval', explode(',', trim($input['p']))); 
                
                $sql->whereBetween('price', $pterm);
            }            

        }

        $products = $sql->orderBy('created_at', 'DESC')->get();

    	return response()->json(['success' => true,
                                'data'     => $products],
                            200);
    }

    public function productDetails($id)
    {

        $product = DB::table('products as p')
                    ->leftJoin('product_options as po', 'po.product_id', '=', 'p.id')
                    ->leftJoin('images as im', 'p.default_image_id', '=', 'im.id')
                    ->leftJoin('categories as c', 'p.category_id', '=', 'c.id')
                    ->leftJoin('brands as b', 'p.brand_id', '=', 'b.id')
                    ->leftJoin('countries as ct', 'p.country_id', '=', 'ct.id')
                    ->leftJoin('measurement as m', 'p.measurement_id', '=', 'm.id')
                    ->leftJoin('variant_options as vo', 'po.variant_option_id', '=', 'vo.id')
                    ->leftJoin('variants as v', 'po.variant_id', '=', 'v.id')
                    ->select('p.*','im.url as image', 'po.sku as skucode', 'po.count','po.reserved_count','po.variant_id', 'po.variant_option_id', 'po.parent_product_option_id', 'c.name as category', 'b.name as brand',
                        'ct.name as origin', 'm.name as unit', 'vo.value as variant_opt', 'v.name as variant')   
                    ->where('p.id', '=', $id)
                    ->first();

        $id = $product->id;

        $data['product'] = $product;

        $data['attributes'] = DB::table('product_attributes as pa')
                                ->leftJoin('products as p', 'pa.product_id', '=', 'p.id')
                                ->join('attributes as a', 'pa.attribute_id', '=', 'a.id')
                                ->select('pa.attribute_id', 'pa.value as attr_val','a.name as attr_name')   
                                ->where('pa.product_id','=', $id)
                                ->get();

        return response()->json(['success' => true,
                                'data'     => $data],
                            200);

    }
}
