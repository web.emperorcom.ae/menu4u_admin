<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Validator,Response;
use Illuminate\Support\Arr;
use DB;
class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }
    public function customers()
    {
        //
       // $users = User::latest()->where('role_id', 1)->get();

        $users = DB::table('users as u')
                    ->leftJoin('emails as e', 'e.user_id','=', 'u.id')
                    ->leftJoin('phones as p', 'p.user_id','=', 'u.id')
                    ->select('e.email', 'u.id', 'u.created_at', 'u.first_name', 'u.last_name', 'u.username', 'u.is_blocked', 'p.number as phone')
                    ->where('u.role_id', 1)
                    ->get();

        return view('users.customers',compact('users'));
                //->with('i', (request()->input('page', 1) - 1) * 5);

    }
      // Change the order status
    public function changeStatus(Request $request){
        $this->validate($request, [      
            'userid'   => 'required',
            'status'   => 'required'    
        ]); 

        $input = $request->all(); 

        $user = User::find($input['userid']);
        $user->is_blocked = $input['status'];
        $user->save();

        if(!$user){
            echo false;
        }
        echo true;

    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $user = DB::table('users as u')
                    ->leftJoin('emails as e', 'e.user_id','=', 'u.id')
                    ->leftJoin('phones as p', 'p.user_id','=', 'u.id')
                    ->select('e.email','e.approved as email_approved', 'p.number as phone', 'p.approved as phone_approved', 'u.*')
                    ->where('u.id', $id)
                    ->first();

        $addresses = DB::table('addresses as a')                        
                        ->leftJoin('countries as c', 'a.country_id', '=', 'c.id')
                        ->select('a.*', 'c.name as country')
                        ->where('a.user_id', '=', $id)
                        ->get();


        return view('users.show',compact('user', 'addresses'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $user = DB::table('users as u')
                    ->leftJoin('emails as e', 'e.user_id','=', 'u.id')
                    ->leftJoin('phones as p', 'p.user_id','=', 'u.id')
                    ->select('e.email', 'p.number as phone', 'u.*')
                    ->where('u.id', $id)
                    ->first();

       
        return view('users.edit',compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request, [
            'first_name' => 'required',
            'last_name'  => 'required',
            'username'   => 'required',
            'email'      => 'required|email',
            'password'   => 'same:confirm-password',
        ]);

        $input = $request->all();        
        $user = User::find($id);

        $insert = ['first_name' => $input['first_name'],
                   'last_name' => $input['last_name'],
                   'username' => $input['username']
                ];
        if(!empty($input['password'])){ 
            $insert['password'] = sha1($input['password']); //Hash::make($input['password']);
        }

        // Update user
        $user->update($insert);

        // Update Email
        if(isset($input['email']) && $input['email']){           
            $email = DB::table('emails')
                        ->where('user_id', $id)
                        ->update(['email' => trim($input['email']) ]);
        }

        // Update phone number
        if(isset($input['phone']) && $input['phone']){
            $phone = DB::table('phones')
                        ->where('user_id', $id)
                        ->update(['number' => trim($input['phone']) ]);
        }
      
        return redirect()->route('users.edit', $id)
                        ->with('success','User updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {    
        //
        DB::table('phones')->where('user_id', $id)->delete();
        DB::table('emails')->where('user_id', $id)->delete();
        DB::table('addresses')->where('user_id', $id)->delete();
        
        User::find($id)->delete();

        return redirect()->route('users.customers')
                        ->with('success','User deleted successfully');
    }
}
