<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use Redirect;
use DB;
use Image;
use Carbon\Carbon;
use Validator,Response,File;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class CategoryController extends Controller
{   

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // 
       $data = DB::table('categories as child')
            ->leftJoin('categories as parent', 'child.category_id', '=', 'parent.id')
            ->select('child.*', 'parent.name as parent')           
            ->orderBy('child.id', 'DESC')->get();
    

        return view('category.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // Parent Categories
        $categories = Category::orderBy('name', 'asc')
                        ->get()
                        ->pluck('name', 'id')
                        ->toArray();  

        return view('category.create',compact( 'categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'name' => 'required',        
            'image_path' => 'file|image|mimes:jpeg,png,jpg,gif,svg|max:2048'            
        ]); 
        
        $input = $request->all();
        // On create
        $img = null;
        if ($request->hasfile('image_path')) {

            $file = $request->file('image_path');
            $response = $this->postImage($_FILES);

            if(isset($response['status']) && $response['status'] == 'SUCCESS'){
                $img = $response['data']['url'];
            }else{
                // error
            }        
        } 

        $lastId = Category::orderBy('id', 'desc')->first()->id;

        Category::create([ 
            'id'        => (int) $lastId + 1,
            'name'      => $input['name'],
            'description' => $input['description'],
            'is_blocked'  => 0,
            'keywords'    => null,
            'image'       => $img,          
            'category_id' => $input['category_id']? $input['category_id'] :0,
            'created_by_user_id' => auth()->user()->id,
            'updated_by_user_id' => auth()->user()->id 
            ]);  
               
        return redirect()->route('categories.index')
                        ->with('success','Category created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       
        // Parent Categories
        $categories = Category::orderBy('name', 'asc')
                        ->get()
                        ->pluck('name', 'id')->toArray(); 

        $category = Category::find($id);


        return view('category.edit',compact( 'categories', 'category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {       
        //
        $this->validate($request, [
            'name' => 'required',           
            'image_path' => 'file|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]); 
        
        $input = $request->all();
        $img = null;

        if ($request->hasfile('image_path')) {

            $file = $request->file('image_path');
            $response = $this->postImage($_FILES);

            if(isset($response['status']) && $response['status'] == 'SUCCESS'){
                $img = $response['data']['url'];
            }else{
                // error
            }        
        } 
        
        $category = Category::find($id);
        $updata = [ 
            'name'        => $input['name'],
            'description' => $input['description'],
            'is_blocked'  => 0,
            'keywords'    => null,
            'image'       => $img? $img: $category->image,          
            'category_id' => $input['category_id']? $input['category_id'] :$category->category_id,
            'created_by_user_id' => $category->created_by_user_id,
            'updated_by_user_id' => auth()->user()->id 
            ];
        
        $category->update($updata);
               
        return redirect()->route('categories.index')
                        ->with('success','Category updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //        
        $category = Category::find($id);

        // Delete Sub Category
        if($category->category_id == 0){ 
            $query = DB::table('categories')->where('category_id', '=', $id);

            if($query->count() >0){
                $subcat = $query->get();
                $query->delete();
            }
        }

        $category->delete(); 

        return redirect()->route('categories.index')
                        ->with('success','Category deleted successfully');

    }

    //  image file upload   
     public function postImage($file){

        $ch = curl_init();
        
        $filePath = $file['image_path']['tmp_name'];
        $fileName = $file['image_path']['name'];
        $type = $file['image_path']['type'];
        $data = array('image' => curl_file_create($filePath, $type, $fileName));         
        
        curl_setopt($ch, CURLOPT_URL, 'https://api.menu4u.ae/api/image');
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,TRUE);
        $response = curl_exec($ch);    
        $err = curl_error($ch);
        curl_close($ch);

        if ($err) {
          return json_decode($err, true);
        } else {
          return json_decode($response, true);
        }
    }

}