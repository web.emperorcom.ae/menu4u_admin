<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\VariantOption;
use App\Variant;
use Validator,Response;
use DB;

class VariantController extends Controller
{
      //
	 /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $variant = DB::table('variant_options as vo')
                        ->leftJoin('variants as v', 'v.id', '=', 'vo.variant_id')
                        ->select('vo.value', 'v.name', 'vo.id')
                        ->orderBy('v.id','DESC')->get();


        return view('variants.index',compact('variant'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $variant = Variant::orderBy('id','DESC')->pluck('name', 'id')->toArray();

        return view('variants.create', compact('variant'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
         /*$this->validate($request, [
                'name' => 'required'
            ]);*/

        $input = $request->all();

        $varId = null;

        // Add new variant
        if(isset($input['name']) && trim($input['name']) != '' && 
            isset($input['variant_id']) && $input['variant_id'] == 0){

            $lastId  = Variant::orderBy('id', 'desc')->first()->id;
            $variant = Variant::create([ 
                        'id'    => (int) $lastId + 1,
                        'name'  => $input['name']
                        ]); 

            $varId = $variant->id;

        }else if(isset($input['variant_id']) && $input['variant_id'] > 0){
            $varId = $input['variant_id'];
        }

        // add variant options
         if(isset($input['option']) && trim($input['option']) != ''){            

            $lastId = VariantOption::orderBy('id', 'desc')->first()->id;

            VariantOption::create([ 
                'id'        => (int) $lastId + 1,
                'value'     => $input['option'],
                'variant_id'=> $varId
                ]); 
        }
        
    
        return redirect()->route('variants.create')
                        ->with('success','Variant created successfully');
    }

    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $variants = Variant::orderBy('id','DESC')->pluck('name', 'id')->toArray();

        $variant = DB::table('variant_options as vo')
                    ->leftJoin('variants as v', 'v.id', '=', 'vo.variant_id')
                    ->select('vo.value as option', 'v.name', 'vo.id', 'vo.variant_id')
                    ->where('vo.id',$id)->first();

        return view('variants.edit',compact( 'variant', 'variants'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $input = $request->all();

        $varId = null;

        // edit  variant
        if(isset($input['variant_id']) && $input['variant_id'] > 0){
            $varId = $input['variant_id'];

            $var = Variant::find($varId);

            if(isset($input['name']) && trim($input['name']) != ''){
                $var->update([ 'name'  => $input['name'] ]); 
            }
        }

        // add variant options
        if(isset($input['option']) && trim($input['option']) != ''){           

            $varoption = VariantOption::find($id);

            $varoption->update([ 
                'value'     => $input['option'],
                'variant_id'=> $varId
                ]); 
        }        
               
        return redirect()->route('variants.index')
                        ->with('success','Variant updated successfully'); 
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //        
        $variantOpt = VariantOption::find($id); 
        // attribute 
        if($variantOpt->variant_id > 0){
            $numofoptions = VariantOption::where('variant_id','=', $variantOpt->variant_id)
                            ->count();

            if($numofoptions < 2){
                Variant::find($variantOpt->variant_id)->delete();
            }
        }
        $variantOpt->delete(); 


        return redirect()->route('variants.index')
                        ->with('success','Variant deleted successfully');

    }

    public function getVariantOptions(Request $request){
    	$data = [];

        $sql = VariantOption::orderBy('id', 'desc');

        if($request->variant_id == 0){
            $sql->whereNull('variant_id');
        }else{
            $sql->where("variant_id", $request->variant_id);
        }
    	$data = $sql->pluck("value", "id")->toArray();

    	return response()->json($data);
    }
   
}
