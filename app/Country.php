<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    //
    protected $fillable = [
    	'id',
        'name',
        'alpha2',
        'alpha3',
        'region',
        'sub_region',
        'dial_code'  
    ];

    
}
