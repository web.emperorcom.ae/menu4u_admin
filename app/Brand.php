<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    //
    protected $fillable = [
    	'id',
		'name',
		'description', 
		'is_blocked',     
		'image',
		'address_id'   
    ];
}
