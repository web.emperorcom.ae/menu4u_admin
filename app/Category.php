<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = [
    	'id',
        'name',
        'description',
        'is_blocked',
        'keywords',
        'image',
        'category_id', 
        'created_by_user_id', 
        'updated_by_user_id'     
    ];

}
