<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductOrder extends Model
{
    //
      protected $fillable = [
        'id',
        'quantity',
        'order_id',
        'product_option_id'
    ];
}
