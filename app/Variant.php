<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Variant extends Model
{
    //
     protected $fillable = [
        'id',
        'name'
    ];

    public function variantoptions()
    {
        return $this->hasMany('App\VariantOption');
    }
}
