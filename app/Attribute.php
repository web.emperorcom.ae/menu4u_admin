<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attribute extends Model
{
 
    protected $fillable = [
        'id',
        'name',
        'default_value',
        'options',        
        'measurement_id'
    ];

    public function measurement()
    {
        return $this->belongsTo('App\Measurement');
    }
}
