<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //
    protected $fillable = [
        'id',
        'sku',
        'barcode',
        'name',
        'is_blocked',
        'description',
        'keywords',
        'price',
        'min_price',
        'unit',
        'step',
        'measurement_id',
        'category_id',
        'country_id',
        'brand_id',
        'default_image_id'
    ];

}