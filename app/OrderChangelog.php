<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderChangelog extends Model
{
    //
    protected $table = 'order_changelog';
    protected $fillable = [
        'id',
        'date',
        'status',
        'order_id',
        'user_id',
    ];
}