<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('/', 'Auth\LoginController@showLoginForm');
Auth::routes();

Route::group(['middleware' => ['auth']], function() {
	Route::get('/home', 'HomeController@index')->name('home');

	Route::resource('/users', 'UserController');
	Route::resource('/measurements', 'MeasurementController');
    Route::resource('/variants', 'VariantController');
    Route::resource('/attributes', 'AttributeController');
	Route::resource('/brands', 'BrandController');
    Route::resource('/categories', 'CategoryController');
    Route::resource('/countries', 'CountryController');
    Route::resource('/products', 'ProductController');
    Route::resource('/options', 'OptionController');

    Route::delete('/deleteproducts', 'ProductController@deleteMultipleProducts')->name('products.multipledelete');
    Route::resource('/orders', 'OrderController');
    Route::get('/print/{id}', 'OrderController@printPreview')->name('orders.print');
    Route::post('/orders/updatestatus', 'OrderController@changeStatus')->name('orders.updatestatus');
    Route::get('/customers', 'UserController@customers')->name('users.customers');

    Route::post('/variantoptions', 'VariantController@getVariantOptions')->name('variant.options');
    Route::post('/fetchattributes', 'MeasurementController@getAttributes')->name('measurement.attributes');

    Route::post('customer/status', 'UserController@changeStatus')->name('user.updatestatus');

    Route::delete('/deletebanners', 'OptionController@deleteBanners')->name('option.bannerdelete');
});

//Route::get('admin/home', 'HomeController@adminHome')->name('admin.home')->middleware('is_admin');

Route::get('/clear-cache', function() {
    Artisan::call('cache:clear');
    Artisan::call('route:clear');
    Artisan::call('config:clear');
    Artisan::call('view:clear');
    // return "Cache is cleared";
    return view('cache');
})->name('cache.clear');