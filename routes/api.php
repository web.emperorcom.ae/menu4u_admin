<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
Route::middleware('auth:api')->group(function () { 
    // our routes to be protected will go in here
   
	Route::post('/verifyUser', 'Api\JWTAuthController@profile');  
    Route::post('/logout', 'Api\JWTAuthController@logout');
    Route::post('/refresh', 'Api\JWTAuthController@refresh');   
});

// PUBLIC
Route::group(['middleware' => ['api', 'cors']], function() {

	Route::get('/banners', 'Api\BannerController@index');
	Route::get('/categories', 'Api\CategoryController@index');
	Route::get('/brands', 'Api\BrandController@index');
	Route::get('/newproducts', 'Api\ProductController@newProducts');
	Route::post('/products', 'Api\ProductController@products');
	Route::get('/productdetails/{id}', 'Api\ProductController@productDetails');

	Route::post('/addorder', 'Api\OrderController@createOrder'); 
	Route::post('/updateorder', 'Api\OrderController@updateOrder');

	Route::post('/register', 'Api\JWTAuthController@register');
	Route::post('/login', 'Api\JWTAuthController@login');


});