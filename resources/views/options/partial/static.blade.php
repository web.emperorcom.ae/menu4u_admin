<div class="col-md-6">
<div class="card card-primary">
  <div class="card-header">
    <h3 class="card-title text-capitalize">Page {{$option->key}}</h3>
  </div>
  <!-- /.card-header -->
  <!-- form start -->

 {!! Form::model($option, ['method' => 'PATCH', 'id' => 'staticfrm', 'route' => ['options.update', $option->id]]) !!}

  {!! Form::hidden('key', null, array('class' => 'form-control')) !!}
  <div class="card-body">

      @if (count($errors) > 0)
      <div class="alert alert-danger">
          <strong>Whoops!</strong> There were some problems with your input.<br><br>
          <ul>
             @foreach ($errors->all() as $error)
               <li>{{ $error }}</li>
             @endforeach
          </ul>
      </div>
      @endif

      <div class="row">
        <div class="col">
          <div class="form-group">
            <label for="title">Title <span class="help-block">*</span></label>
            {!! Form::text('title', $data[0]['title'], array('placeholder' => 'Privacy Title','class' => 'form-control', 'id' => 'title')) !!}
          </div> 
        </div>
      </div>
      <div class="row">
        <div class="col">                 
          <div class="form-group">
            <label for="description">Description</label>
            {!! Form::textarea('description',$data[0]['content'], ['placeholder' => 'Description','class' => 'form-control','id' => 'description', 'rows' => 5, 'cols' => 56]) !!}
          </div> 
        </div>
      </div>
  </div>
  <!-- /.card-body -->
  <div class="card-footer">
    <p class="text-danger d-none" id="upmsg">Please wait, upload will take few minutes...</p>
    <button type="submit" class="btn btn-primary float-right">Submit</button>
  </div>
{!! Form::close() !!}
</div>
<div class="col-md-6">&nbsp;</div>
<script type="text/javascript"> 
$(document).ready(function(){

   // form submit
   $("#staticfrm").on("submit", function(e){
     $('#upmsg').removeClass('d-none');
     return true;
   });
}); 