<div class="col-md-6">
<div class="card card-primary">
  <div class="card-header">
    <h3 class="card-title">Upload Banners</h3>
  </div>
  <!-- /.card-header -->
  <!-- form start -->

 {!! Form::model($option, ['method' => 'PATCH', 'id' => 'bannerfrm', 'enctype' => 'multipart/form-data', 'route' => ['options.update', $option->id]]) !!}

  {!! Form::hidden('key', null, array('class' => 'form-control')) !!}
  <div class="card-body">

      @if (count($errors) > 0)
      <div class="alert alert-danger">
          <strong>Whoops!</strong> There were some problems with your input.<br><br>
          <ul>
             @foreach ($errors->all() as $error)
               <li>{{ $error }}</li>
             @endforeach
          </ul>
      </div>
      @endif
      <div class="row">
        <div class="col">
          <div class="form-group">
              <label>Upload Image:</label>
              <div class="input-group">
                  <div class="custom-file">
                      <input type="file" class="custom-file-input" id="file-input" name="image_path" accept="image/svg,image/jpeg,image/jpg,image/png,image/gif"/>
                      <label class="custom-file-label" for="file-input">Choose file</label>
                  </div>
                  <div class="input-group-append">
                      <span class="input-group-text" id="">Upload</span>
                  </div>
              </div>
              <p class="small text-muted mt-1">Image should be in any of the format (jpeg,png,jpg,gif,svg)</p>
              <div id="file-error" class="text-danger mt-1"></div>
              <span class="text-danger">{{ $errors->first('image_path') }}</span>           
          </div>
          <div class="form-group">
              <div id="thumb-output"></div>
            </div> 
          </div>
          <div class="col">
            <div class="form-group">
              <label>Product :</label>
                <select class="form-control" name="productId" id="product" required="">
                  <option value="">-- Select --</option>                                
                  @foreach ($products as $key => $value)
                    <option value="{{ $key }}"> 
                        {{ $value }} 
                    </option>
                  @endforeach
                </select>
            </div>
          </div>
      </div> 
   
  </div>
  <!-- /.card-body -->
  <div class="card-footer">
    <p class="text-danger d-none" id="upmsg" >Please wait, upload will take few minutes...</p>
    <button type="submit" class="btn btn-primary float-right" >Submit</button>
  </div>
{!! Form::close() !!}
</div>
<!-- /.card -->
</div>
<div class="col-md-6">
  <div class="card card-secondary">
    <div class="card-header">
      <h3 class="card-title">List Banners</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
  <div class="card-body">
    @foreach($data as $i => $ban)
      <div class="row" id="bimg{{$i}}">
        <div class="col">
          @if($ban['image'])
            @foreach ($products as $key => $value)
              @if($ban['productId'] == $key)<h5> {{ $value }}</h5> @endif
            @endforeach
            <div class="d-inline">    
              <img src="https://api.menu4u.ae/api/image/{{$ban['image']}}" class="img-fluid img-thumbnail m-1 mht-100">
            </div>
          @endif
        </div> 
        <div class="col-2">
          <a href="javascript:void(0);" rel="{{$i}}" class="btn btn-danger del-image" title="Delete">
          <i class="far fa-trash-alt"></i> </a>
        </div>        
      </div>
      <div class="row"><div class="col"><hr/></div></div>
      @endforeach
  </div>

  </div>
<!-- /.card -->
</div>

<script type="text/javascript"> 
$(document).ready(function(){

  // image upload
 $('#file-input').on('change', function(){ //on file input change
    if (window.File && window.FileReader && window.FileList && window.Blob) //check File API supported browser
    {
        var data = $(this)[0].files; //this file data
         
        $.each(data, function(index, file){ //loop though each file
            if(/(\.|\/)(gif|jpe?g|png|svg)$/i.test(file.type)){ //check supported file type
                var fRead = new FileReader(); //new filereader
                fRead.onload = (function(file){ //trigger function on successful read
                return function(e) {
                    var img = $('<img/>').addClass('img-fluid img-thumbnail m-1 mht-100').attr('src', e.target.result); //create image element 
                   
                    $('#thumb-output').html(img); //append image to output element
                };
                })(file);
                fRead.readAsDataURL(file); //URL representing the file's data.
                $('#file-error').html("");
            }else{              
                $('#file-error').html("Selected file extension not allowed");
                return;
            };
        });
         
    }else{
        alert("Your browser doesn't support File API!"); //if File API is absent
    }
 });

 // form submit
 $("#bannerfrm").on("submit", function(e){
   $('#upmsg').removeClass('d-none');
   return true;
 });

 // Ajax Call
  $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  }); 

  // Delete banner image 
  $('.del-image').on('click', function(e){
    let banid = $(this).attr('rel');
    let optid = {{$option->id}};

    if(!banid) return;

    $.ajax({
      url: "https://admin.menu4u.ae/deletebanners",
      type: 'DELETE',       
      data: 'id='+banid+'&optid='+optid, 
      success: function (data) {

        if(data.status == true){
          $('#bimg'+banid).addClass('d-none');

          toastr.success(data.message); 
        }else{
          toastr.error(data.message); 
        }
      },
      error: function (data) {
        
        toastr.error(data.responseJSON.errors); 

      }
    });  
    

  });
});
</script>