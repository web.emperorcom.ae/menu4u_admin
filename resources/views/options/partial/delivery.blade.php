<!-- general form elements -->
<div class="card card-primary">
  <div class="card-header">
    <h3 class="card-title">Shipping Cost</h3>
  </div>
  <!-- /.card-header -->
  <!-- form start -->
  
   {!! Form::model($option, ['method' => 'PATCH','route' => ['options.update', $option->id]]) !!}
  
    {!! Form::hidden('key', null, array('class' => 'form-control')) !!}
    <div class="card-body">

        @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
               @foreach ($errors->all() as $error)
                 <li>{{ $error }}</li>
               @endforeach
            </ul>
        </div>
        @endif
        <div class="row">
         
            <div class="col-xs-12 col-sm-12 col-md-12">
                @foreach($cities as $citi)
                <table class="table  table-hover">
                  <tr>
                    <td width="50%"><strong>{{ $citi }}</strong></td>
                    <td width="50%">
                    {!! Form::number('price[]', null, array('placeholder' => 'Cost','class' => 'form-control')) !!}</td>
                    <td>{!! Form::text('city', $data->city, array('placeholder' => 'City','class' => 'form-control')) !!}

                    </td>
                  </tr>
                </table>
                @endforeach
            </div>
            
        </div>
    </div>
    <!-- /.card-body -->
    <div class="card-footer">
      <button type="submit" class="btn btn-primary float-right">Submit</button>
    </div>
  {!! Form::close() !!}
</div>
<!-- /.card -->
