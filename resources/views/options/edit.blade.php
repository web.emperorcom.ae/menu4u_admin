@extends('layouts.admin')

@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Edit Settings</h1>

      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
          <li class="breadcrumb-item active">Edit Settings</li>
        </ol>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-6">
        @if ($message = Session::get('success'))
          <div class="alert alert-success">
              <p>{{ $message }}</p>
          </div>    
        @endif
      </div>
      <div class="col-md-6">
        <a class="btn btn-secondary float-right mb-2" href="{{ route('options.index') }}"> Back</a>
      </div>
    </div>
     
    <div class="row">
      <!-- left column -->
    @if($option->key == 'home_carousel')      
      @include("options.partial.banner")    
    @endif
    @if($option->key == 'privacy' || $option->key == 'terms')      
      @include("options.partial.static")    
    @endif
    
    </div>
    <div class="row">
      <div class="col-md-12">&nbsp;</div>
    </div>
  </div>
</section>

@endsection
