 <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-warning elevation-4">
    <!-- Brand Logo -->
    <a href="{{ url('/home') }}" class="brand-link">
      <img src="{{ asset('./images/logo-icon.png') }}" alt="Menu4u Logo" class="brand-image ">
      <span class="brand-text font-weight-light">Menu4u Admin</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{ asset('./dist/img/avatar3.png') }}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="{{ url('/home') }}" class="d-block">{{ Auth::user()->first_name }}</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
         <li class="nav-item">
            <a href="{{ url('/home') }}" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt mr-2"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li> 
           <li class="nav-item ">
            <a href="{{ route('countries.index') }}" class="nav-link">
              <i class="fas fa-flag mr-2"></i>
              <p>
                Country
              </p>
            </a>            
          </li>
           <li class="nav-item ">
            <a href="{{ route('measurements.index') }}" class="nav-link">
              <i class="fas fa-weight mr-2"></i>
              <p>
                Measurements
              </p>
            </a>            
          </li>
            <li class="nav-item ">
            <a href="{{ route('attributes.index') }}" class="nav-link">
              <i class="fas fa-asterisk mr-2"></i>
              <p>
                Attributes
              </p>
            </a>            
          </li>
          <li class="nav-item ">
            <a href="{{ route('variants.index') }}" class="nav-link">
              <i class="fas fa-check-double mr-2"></i>
              <p>
                Variants
              </p>
            </a>            
          </li>    
          <li class="nav-item ">
            <a href="{{ route('categories.index') }}" class="nav-link">
              <i class="fas fa-stream mr-2"></i>
              <p>
                Category
              </p>
            </a>            
          </li>
          <li class="nav-item ">
            <a href="{{ route('brands.index') }}" class="nav-link">
              <i class="fas fa-tags mr-2"></i>
              <p>
                Brands
              </p>
            </a>            
          </li> 
           <li class="nav-item ">
            <a href="{{ route('products.index') }}" class="nav-link">
              <i class="fas fa-shopping-basket mr-2"></i>
              <p>
                Product
              </p>
            </a>            
          </li>
           <li class="nav-item">
            <a href="{{ route('orders.index') }}" class="nav-link">
              <i class="fas fa-cart-arrow-down mr-2"></i>
              <p>
                Order
              </p>
            </a>            
          </li>
          <li class="nav-item">
            <a href="{{ route('users.customers') }}" class="nav-link">
              <i class="fas fa-users mr-2"></i>
              <p>
                Customers
              </p>
            </a>            
          </li>
          <li class="nav-item">
            <a href="{{ route('options.index') }}" class="nav-link">
             <i class="fas fa-cog mr-2"></i>
              <p>
                Settings
              </p>
            </a>            
          </li>
          <li class="nav-item ">
            <a href="{{ route('cache.clear') }}" class="nav-link">
              <i class="fas fa-broom mr-2"></i> 
              <p>
                Clear Cache
              </p>
            </a>            
          </li>  
          <li class="nav-item">
            <a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();"  class="nav-link">
              <i class="nav-icon fas fa-sign-out-alt mr-2"></i>
              <p>
                Logout
              </p>
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
               @csrf
            </form>
          </li>        
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>