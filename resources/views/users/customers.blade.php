@extends('layouts.admin')


@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Customers</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
          <li class="breadcrumb-item active">Customer</li>
        </ol>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col">

        <div class="card">
        <div class="card-header">
          <h3 class="card-title">Manage Customers</h3>                  
        </div>
        <!-- /.card-header -->
        <div class="card-body">
          @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
          @endif          
          <table id="customer-tbl" class="table table-bordered table-hover">
            <thead>
            <tr>
              <th>#</th>
              <th>Name</th>
              <th>Email</th>
              <th>Phone</th>
              <th>Is Blocked</th>
              <th>Joined at</th>
              <th>Action</th>                  
            </tr>
            </thead>
            <tbody> 
             @foreach ($users as $key => $user)
              @php
                $time = strtotime($user->created_at) + 60*60*4;
              @endphp 
              <tr>
                <td>{{ $key + 1 }}</td>
                <td>{{ $user->first_name }} {{ $user->last_name }}</td>
                <td>{{ $user->email }}</td>
                <td>{{ $user->phone }}</td>
                <td>
                <input type="checkbox" name="user-checkbox" data-bootstrap-switch data-off-color="danger" data-on-color="success" {{ ($user->is_blocked == true)? 'checked' : ''}} value={{ $user->id }} />
                </td>
                <td>{{ date("d-m-Y H:i:s", $time) }}</td>
                <td>
                  <a class="btn btn-warning mr-2" href="{{ route('users.show',$user->id) }}" title="View"><i class="fas fa-eye"></i></a>  
                  <!-- <a class="btn btn-warning mr-2" href="{{ route('users.edit',$user->id) }}" title="Edit"><i class="fas fa-edit"></i></a>
                    -->
                    {!! Form::open(['method' => 'DELETE','route' => ['users.destroy', $user->id],'style'=>'display:inline']) !!}
                      <button type="submit" class="btn btn-danger" title="Delete"><i class="fas fa-trash"></i></button>
                    {!! Form::close() !!}     
                            
                </td>
              </tr>
             @endforeach 
            </tbody>
            <tfoot>
            <tr>
              <th>#</th>
              <th>Name</th>
              <th>Email</th>
              <th>Phone</th>
              <th>Is Blocked</th>
              <th>Joined at</th>
              <th>Action</th>                       
            </tr>
            </tfoot>
          </table>
        </div>
        <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
      <!-- <div class="col">&nbsp;</div> -->
    </div>
  </div>
</section> 
<!-- /.content -->
<script type="text/javascript">
  $(document).ready(function(){  
    
    $("input[data-bootstrap-switch]").each(function(){
        $(this).bootstrapSwitch('state', $(this).prop('checked'));
    });

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('input[data-bootstrap-switch]').on('switchChange.bootstrapSwitch', function (e, ischeck) {
       
        if(!$(this).val()){return false;}
        let obj = $(this);
        
        $.ajax({
            url:"https://admin.menu4u.ae/customer/status",
            method:"POST",
            data:{userid: obj.val(), status: (ischeck? true: false)},
            success:function(data){
             // console.log('Success', data);
              obj.bootstrapSwitch('state', ischeck);
            },
            error:function(data){ 
              //console.log('Error', data);
            }
        });
    });
  });
</script>
@endsection