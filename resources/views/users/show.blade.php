@extends('layouts.admin')


@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Customer Details</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
          <li class="breadcrumb-item active">View Customer</li>
        </ol>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-6">
        
        @php
            $time = strtotime($user->created_at) + 60*60*4;
        @endphp 
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">
              <i class="fas fa-info-circle"></i>
              Basic Details
            </h3>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <dl class="row">
              <dt class="col-sm-4">User ID</dt>
              <dd class="col-sm-8"># {{$user->id}}</dd>
              <dt class="col-sm-4">Name</dt>
              <dd class="col-sm-8">{{$user->first_name.' '.$user->last_name}}</dd>
              <dt class="col-sm-4">Username</dt>
              <dd class="col-sm-8">{{$user->username?$user->username:'--'}}</dd>
              <dt class="col-sm-4">Email</dt>
              <dd class="col-sm-8">{{$user->email}} 
              @if($user->email) 
                @if($user->email_approved) <span class="text-success ml-3"  title="Approved"><i class="far fa-check-circle"></i>&nbsp;Approved</span>
                @else <span class="text-danger ml-3"  title="Not Approved"><i class="far fa-times-circle text-danger"></i>&nbsp;Not Approved</span> @endif
              @else
                --
              @endif
              </dd>
              <dt class="col-sm-4">Phone</dt>
              <dd class="col-sm-8">{{$user->phone}}
              @if($user->phone) 
                @if($user->phone_approved ) <span class="text-success ml-3"  title="Approved"><i class="far fa-check-circle mr-2"></i>Approved</span>
                @else <span class="text-danger ml-3"  title="Not Approved"><i class="far fa-times-circle text-danger mr-2"></i>Not Approved</span> @endif
              @else
                --
              @endif
              </dd>             
            </dl>
          </div>
          <!-- /.card-body -->
        </div>        
      </div><!-- /.col -->
      <div class="col-md-6">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">
              <i class="fas fa-text-width"></i>
              Additional Details
            </h3>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            @php
                $time = strtotime($user->created_at) + 60*60*4;
            @endphp 
            <dl class="row">
              <dt class="col-sm-4">Is Blocked</dt>
              <dd class="col-sm-8">{{($user->is_blocked ==  true)? 'Yes' : 'No'}}</dd>
              <dt class="col-sm-4">Joined On</dt>
              <dd class="col-sm-8">{{date("d-m-Y H:i:s", $time)}}</dd>
              <dt class="col-sm-4">Facebook ID</dt>
              <dd class="col-sm-8">{{$user->facebook_id? $user->facebook_id: '--'}}</dd>
              <dt class="col-sm-4">Firebase ID</dt>
              <dd class="col-sm-8">{{$user->firebase_id? $user->firebase_id: '--'}}</dd>
              <dt class="col-sm-4">Google ID</dt>
              <dd class="col-sm-8">{{$user->google_id? $user->google_id: '--'}}
              </dd>             
            </dl>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
    </div><!-- /.row -->
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">
              <i class="fas fa-map-marker-alt"></i>
              Address
            </h3>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <dl class="row">
              @if(count($addresses) == 0)               
                <dt class="col-sm-12"><p class="text-center">No address</p> </dt>                
              @endif
              @foreach($addresses as $key => $address)
              
                <dt class="col-sm-4">Address 1</dt>
                <dd class="col-sm-8">{{$address->line1}}</dd> 
                <dt class="col-sm-4">Address 2</dt>
                <dd class="col-sm-8">{{$address->line2? $address->line2 : '--'}}</dd> 
                <dt class="col-sm-4">City</dt>
                <dd class="col-sm-8">{{$address->city?$address->city: '--'}}</dd> 
                <dt class="col-sm-4">Region</dt>
                <dd class="col-sm-8">{{$address->region?$address->region:'--'}}</dd>
                <dt class="col-sm-4">Country</dt>
                <dd class="col-sm-8">{{$address->country?$address->country: '--'}}</dd>
                <dt class="col-sm-4">Zip</dt>
                <dd class="col-sm-8">{{$address->zip?$address->zip: '--'}}</dd>
                @if(count($addresses) != ($key+1))
                  <dt class="col-sm-12"><hr/></dt>
                @endif
              @endforeach            
            </dl>
          </div>
          <!-- /.card-body -->
          <div class="card-footer">
            <a class="btn btn-secondary float-right" href="{{url()->previous()}}">BACK</a>
          </div>
        </div>
        <!-- /.card -->
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">&nbsp;</div>
    </div>
  </div><!-- /.container-fluid -->
</section>
@endsection