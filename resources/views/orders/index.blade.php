@extends('layouts.admin')


@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Orders</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
          <li class="breadcrumb-item active">Orders</li>
        </ol>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col">

        <div class="card">
        <div class="card-header">
          <h3 class="card-title">Manage Orders</h3>                  
        </div>
        <!-- /.card-header -->
        <div class="card-body">
          @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
          @endif          
          <table id="order-tbl" class="table table-bordered table-hover">
            <thead>
            <tr>
              <th>#</th>
              <th>Name</th>
              <th>Status</th>
              <th>Comment</th>                        
              <th>Created at</th>              
              <th>Action</th>                  
            </tr>
            </thead>
            <tbody id="order-new"> 
             @foreach ($orders as $key => $order)
              @php
                $time = strtotime($order->created_at) + 60*60*4;
              @endphp 
              <tr>
                <td>{{ $order->id }}</td>
                <td>{{ $order->first_name.' '.$order->last_name }}</td>
                <td><span class="badge badge-danger">{{$order->status}}</span>
                </td> 
                <td>{{ $order->comment }}</td>                
                <td>{{ date("d-m-Y H:i:s", $time) }}</td>                            
                <td>             
                   <a href="{{route('orders.print', $order->id)}}" target="_blank" title="Print Preview" class="btn btn-info mr-2"><i class="fas fa-print"></i></a>

                   <a class="btn btn-warning mr-2" href="{{ route('orders.show',$order->id) }}" title="View"><i class="fas fa-eye"></i></a>  
                  {!! Form::open(['method' => 'DELETE','route' => ['orders.destroy', $order->id],'style'=>'display:inline']) !!}
                    <button type="submit" class="btn btn-danger" title="Delete" onclick="return confirmDel('{{ $order->id }}' );"><i class="fas fa-trash"></i></button>
                  {!! Form::close() !!}           
                </td>
              </tr>
             @endforeach 
            </tbody>
            <tfoot>
            <tr>
              <th>#</th>
              <th>Name</th>
              <th>Status</th>
              <th>Comment</th>                        
              <th>Created at</th>              
              <th>Action</th>                     
            </tr>
            </tfoot>
          </table>
        </div>
        <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
      <!-- <div class="col">&nbsp;</div> -->
    </div>
  </div>
</section> 
<!-- /.content -->
<script type="text/javascript">
function confirmDel(name){
  if(confirm('Are you sure to delete the order of #'+name+' ?')){
    return true;
  }else{
    return false;
  }
}
</script>
@endsection