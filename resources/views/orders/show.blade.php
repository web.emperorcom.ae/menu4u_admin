@extends('layouts.admin')


@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Order Summary</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
          <li class="breadcrumb-item active">View Order</li>
        </ol>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>
  <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="callout callout-info">
              <h5><i class="fas fa-info"></i> Note:</h5>
              Please check the order details and update status of it.
              @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <p>{{ $message }}</p>
                </div>
              @endif 
            </div>
            @php
                $time = strtotime($order->created_at) + 60*60*4;
            @endphp 
            <!-- Main content -->
            <div class="invoice p-3 mb-3">
              <!-- title row -->
              <div class="row">
                <div class="col-12">
                  <h4>
                    <i class="fas fa-globe"></i> Menu4u
                    <small class="float-right">Date: {{date("d-m-Y H:i:s", $time)}}</small>
                  </h4>
                </div>
                <!-- /.col -->
              </div>
              <!-- info row -->
              <div class="row invoice-info">
                <div class="col-sm-4 invoice-col">
                  From
                  <address>
                    <strong>{{$order->first_name.' '.$order->last_name}}</strong><br>
                    @if($address)
                      {{$address->line1}}, {{$address->line2}}<br>
                      {{$address->city}}, {{$address->region}}, {{$address->zip}}<br>
                    @endif
                    @if($phones)
                      Phone:  {{$phones->number}}<br>
                    @endif
                    @if($emails)
                      Email:  {{$emails->email}}
                    @endif
                  </address>
                </div>
                <!-- /.col -->
                <div class="col-sm-4 invoice-col">
                 
                </div>
                <!-- /.col -->
                <div class="col-sm-4 invoice-col">                 
                  <b>Order ID:</b> {{$order->id}}<br>
                  <b>Payment:</b> Cash On Delivery<br>
                  <b>Delivery Type:</b> Shipping<br>
                  <b>Comments:</b> {{$order->comment}}
                  <br/>
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
              @if(count($products) == 0)
              <div class="row">
                <div class="col-12">
                  <p class="text-muted text-center">  No Products Found</p>
                </div>
              </div>
              @else
              <!-- Table row -->
              <div class="row">
                <div class="col-12 table-responsive">
                  <table class="table table-striped">
                    <thead>
                    <tr>
                      <th>Qty</th>
                      <th>Product</th>                     
                      <th>Subtotal</th>
                    </tr>
                    </thead>
                    <tbody>
                      @php
                        $sub_total = 0;
                      @endphp                     
                      @foreach($products as $row)
                      @php
                        $item_total = ($row->quantity * $row->price);
                        $sub_total += $item_total;
                      @endphp                     
                      <tr>
                        <td>{{$row->quantity}}</td>
                        <td>{{$row->name}}    
                        </td>                     
                        <td>AED {{$item_total}}</td>
                      </tr>
                     
                      @endforeach  
                              
                    </tbody>
                  </table>
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->

              <div class="row">
                <!-- accepted payments column -->
                <div class="col-6">
                 <!--  <p class="lead">Payment Methods:</p>
                  <img src="../../dist/img/credit/visa.png" alt="Visa">
                  <img src="../../dist/img/credit/mastercard.png" alt="Mastercard">
                  <img src="../../dist/img/credit/american-express.png" alt="American Express">
                  <img src="../../dist/img/credit/paypal2.png" alt="Paypal">

                  <p class="text-muted well well-sm shadow-none" style="margin-top: 10px;">
                    Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles, weebly ning heekya handango imeem
                    plugg
                    dopplr jibjab, movity jajah plickers sifteo edmodo ifttt zimbra.
                  </p> -->
                </div>
                <!-- /.col -->
                <div class="col-6">                  
                  <div class="table-responsive">
                    <table class="table">
                      <tr>
                        <th style="width:50%">Price</th>
                        <td>AED {{$sub_total}}</td>
                      </tr>                      
                      <tr>
                        <th>VAT (5%)</th>
                        <td>
                        @php
                          $vat_price = $sub_total * 0.05;
                        @endphp  

                        AED {{$vat_price}}</td>
                      </tr>
                     
                      <tr>
                        <th>Delivery Charge</th>
                        <td>FREE</td>
                      </tr>
                      <tr>
                        <th>Total</th>
                        <td>AED {{$sub_total + $vat_price}}</td>
                      </tr>
                    </table>
                  </div>
                </div>
                <!-- /.col -->
              </div>
               @endif      

              <!-- this row will not appear when printing -->
              <div class="row no-print">
                <div class="col-12">
                 
                  <!-- <button type="button" class="btn btn-success float-right"><i class="far fa-credit-card"></i> Submit
                    Payment
                  </button>
                  <button type="button" class="btn btn-primary float-right" style="margin-right: 5px;">
                    <i class="fas fa-download"></i> Generate PDF
                  </button> -->
                </div>
              </div>
            </div>
            <!-- /.invoice -->
              <!-- /.row -->
              <div class="row">
                <!-- accepted payments column -->
                <div class="col-12">
                  <div class="card card-primary card-outline card-outline-tabs">     
                    <div class="card-header">
                      <h4>Update Order Status</h4>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                    <div class="row">
                    <div class="col-6"> 
                    {!! Form::open(array('route' => 'orders.updatestatus','method'=>'POST', 'autocomplete' => 'off' )) !!}

                      @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <strong>Whoops!</strong> There were some problems with your input.<br><br>
                            <ul>
                               @foreach ($errors->all() as $error)
                                 <li>{{ $error }}</li>
                               @endforeach
                            </ul>
                        </div>
                        @endif
                      <input type="hidden" name="order_id" value="{{$order->id}}">
                      <div class="form-group">
                        <label for="order_status_id">Status</label>
                        <select class="form-control" name="order_status_id" id="order_status_id">   
                          <option value="">-- Select --</option>                
                            @foreach ($status_lists as $id => $name)
                              <option value="{{$id}}" {{$id == $order->order_status_id? 'selected': ''}}>{{$name}}</option> 
                            @endforeach
                        </select>
                      </div>
                      <div class="form-group">
                        <label for="comments">Comments</label>                     
                        {!! Form::textarea('comments', null, ['placeholder' => 'If any comments, please enter','class' => 'form-control','id' => 'comments', 'rows' => 4, 'cols' => 10]) !!}
                      </div>

                      <button type="submit" class="btn btn-primary">Submit</button>

                    {!! Form::close() !!}
                  </div>
                    <div class="col-6"> 
                    <div class="table-responsive">
                      <table class="table">
                        <tr>
                          <th>Status</th>   
                          <th style="width:50%">Comments</th>                                     
                          <th>Date On</th>                       
                        </tr>
                        @if(count($history) == 0)
                          <tr>
                          <td colspan="3"><p class="text-muted text-center">No Data</p></td>
                          </tr>
                        @endif
                        @foreach($history as $his)
                          @php
                            $time = strtotime($his->created_at) + 60*60*4;
                          @endphp 
                        <tr>
                          <td>{{$his->status_name}}</td>
                          <td>{{$his->status}}</td>
                          <td>{{date("d-m-Y H:i:s", $time)}}</td>
                        </tr> 
                        @endforeach
                                
                      </table>
                    </div>
                  </div>
                  </div><!-- row -->
                </div>
                  <div class="card-footer">                      
                      <a class="btn btn-secondary float-right" href="{{ route('orders.index') }}"> Back</a>                      
                    </div>
                  </div>
                </div>
              
              </div>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>


@endsection