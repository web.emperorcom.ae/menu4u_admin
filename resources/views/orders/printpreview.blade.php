@extends('layouts.admin')


@section('content')

  <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="callout callout-info">
              <h5><i class="fas fa-info"></i> Note:</h5>
              Please check the order details        
            </div>
            @php
                $time = strtotime($order->created_at) + 60*60*4;
            @endphp 
            <!-- Main content -->
             <!-- Main content -->
            <div class="invoice p-3 mb-3">
              <!-- title row -->
              <div class="row">
                <div class="col-12">
                  <h4>
                    <i class="fas fa-globe"></i> Menu4u
                    <small class="float-right">Date: {{date("d-m-Y H:i:s", $time)}}</small>
                  </h4>
                </div>
                <!-- /.col -->
              </div>
              <!-- info row -->
              <div class="row invoice-info">
                <div class="col-sm-4 invoice-col">
                  From
                  <address>
                    <strong>{{$order->first_name.' '.$order->last_name}}</strong><br>
                    @if($address)
                      {{$address->line1}}, {{$address->line2}}<br>
                      {{$address->city}}, {{$address->region}}, {{$address->zip}}<br>
                    @endif
                    @if($phones)
                      Phone:  {{$phones->number}}<br>
                    @endif
                    @if($emails)
                      Email:  {{$emails->email}}
                    @endif
                  </address>
                </div>
                <!-- /.col -->
                <div class="col-sm-4 invoice-col">
                 
                </div>
                <!-- /.col -->
                <div class="col-sm-4 invoice-col">                 
                  <b>Order ID:</b> {{$order->id}}<br>
                  <b>Payment:</b> Cash On Delivery<br>
                  <b>Delivery Type:</b> Shipping<br>
                  <b>Comments:</b> {{$order->comment}}
                  <br/>
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
              @if(count($products) == 0)
              <div class="row">
                <div class="col-12">
                  <p class="text-muted text-center">  No Products Found</p>
                </div>
              </div>
              @else
              <!-- Table row -->
              <div class="row">
                <div class="col-12 table-responsive">
                  <table class="table table-striped">
                    <thead>
                    <tr>
                      <th>Qty</th>
                      <th>Product</th>                     
                      <th>Subtotal</th>
                    </tr>
                    </thead>
                    <tbody>
                      @php
                        $sub_total = 0;
                      @endphp                     
                      @foreach($products as $row)
                      @php
                        $item_total = ($row->quantity * $row->price);
                        $sub_total += $item_total;
                      @endphp                     
                      <tr>
                        <td>{{$row->quantity}}</td>
                        <td>{{$row->name}}    
                        </td>                     
                        <td>AED {{$item_total}}</td>
                      </tr>
                     
                      @endforeach  
                              
                    </tbody>
                  </table>
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->

              <div class="row">
                <!-- accepted payments column -->
                <div class="col-6">
                 <!--  <p class="lead">Payment Methods:</p>
                  <img src="../../dist/img/credit/visa.png" alt="Visa">
                  <img src="../../dist/img/credit/mastercard.png" alt="Mastercard">
                  <img src="../../dist/img/credit/american-express.png" alt="American Express">
                  <img src="../../dist/img/credit/paypal2.png" alt="Paypal">

                  <p class="text-muted well well-sm shadow-none" style="margin-top: 10px;">
                    Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles, weebly ning heekya handango imeem
                    plugg
                    dopplr jibjab, movity jajah plickers sifteo edmodo ifttt zimbra.
                  </p> -->
                </div>
                <!-- /.col -->
                <div class="col-6">                  
                  <div class="table-responsive">
                    <table class="table">
                      <tr>
                        <th style="width:50%">Price</th>
                        <td>AED {{$sub_total}}</td>
                      </tr>                      
                      <tr>
                        <th>VAT (5%)</th>
                        <td>
                        @php
                          $vat_price = $sub_total * 0.05;
                        @endphp  

                        AED {{$vat_price}}</td>
                      </tr>
                     
                      <tr>
                        <th>Delivery Charge</th>
                        <td>FREE</td>
                      </tr>
                      <tr>
                        <th>Total</th>
                        <td>AED {{$sub_total + $vat_price}}</td>
                      </tr>
                    </table>
                  </div>
                </div>
                <!-- /.col -->
              </div>
               @endif  
            

              <!-- this row will not appear when printing -->
              <div class="row no-print">
                <div class="col-12">
                  <a href="#" target="_blank" class="btn btn-default" onclick="return window.print()"><i class="fas fa-print"></i> Print</a>
                
                </div>
              </div>
            </div>
            <!-- /.invoice -->
              <!-- /.row -->
            
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>

<SCRIPT LANGUAGE="JavaScript"> 
// if (window.print) {
//     document.write('<form><input type=button name=print value="Print Page"onClick="window.print()"></form>');
// }
</script>
@endsection