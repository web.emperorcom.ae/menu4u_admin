@extends('layouts.admin')


@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Edit Country</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
          <li class="breadcrumb-item"><a href="{{ route('countries.index') }}">Country</a></li>
          <li class="breadcrumb-item active">Edit Country</li>
        </ol>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
      <div class="row">
        <!-- left column -->
     
      <div class="col">
        <!-- general form elements -->
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Edit Country</h3>
          </div>
          <!-- /.card-header -->
          <!-- form start -->
            {!! Form::model($country, ['method' => 'PATCH', 'enctype' => 'multipart/form-data', 'route' => ['countries.update', $country->id]]) !!}          
            <div class="card-body">
                @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                       @foreach ($errors->all() as $error)
                         <li>{{ $error }}</li>
                       @endforeach
                    </ul>
                </div>
                @endif
                <div class="row">
                <div class="form-group col-6">
                    <label for="c_name">Country Name <span class="help-block">*</span></label>
                    {!! Form::text('name', null, array('placeholder' => 'Country Name','class' => 'form-control', 'id' => 'c_name')) !!}
                </div> 

                <div class="form-group  col-6">
                    <label>Short Name (2 letter):</label>
                    {!! Form::text('alpha2', null, array('placeholder' => 'Short Name','class' => 'form-control', 'id' => 'alpha2')) !!}                  
                </div>   
                </div>  
                <div class="row">
                <div class="form-group col-6">
                     <label>Short Name (3 letter):</label>
                    {!! Form::text('alpha3', null, array('placeholder' => 'Short Name','class' => 'form-control', 'id' => 'alpha3')) !!}
                </div> 

                <div class="form-group  col-6">
                    <label>Region:</label>
                    {!! Form::text('region', null, array('placeholder' => 'Region','class' => 'form-control', 'id' => 'region')) !!}                  
                </div>   
                </div>  
                <div class="row">
                <div class="form-group col-6">
                     <label>Sub Region:</label>
                    {!! Form::text('sub_region', null, array('placeholder' => 'Sub Region','class' => 'form-control', 'id' => 'sub_region')) !!}
                </div> 

                <div class="form-group  col-6">
                    <label>Dial Code:</label>
                    {!! Form::text('dial_code', null, array('placeholder' => 'Dial Code','class' => 'form-control', 'id' => 'dial_code')) !!}                  
                </div>   
                </div>      
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
              <button type="submit" class="btn btn-primary">Submit</button>
              <a class="btn btn-secondary float-right" href="{{ route('countries.index') }}"> Cancel</a>
            </div>
          {!! Form::close() !!}
        </div>
        <!-- /.card -->
    </div>
    
    </div>
    </div>
</section>

@endsection