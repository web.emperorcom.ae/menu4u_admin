@extends('layouts.admin')


@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Country</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
          <li class="breadcrumb-item active">Manage Countries</li>
        </ol>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>
<!-- Main content -->
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col">
        <div class="card">
        <div class="card-header">
          <h3 class="card-title">Manage Countries</h3>          
            <!-- <a class="btn bg-gradient-success float-right" href="{{ route('countries.create') }}" title="Add New">CREATE NEW</a> -->
        </div>
        <!-- /.card-header -->
        <div class="card-body">
          @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
          @endif
          <table id="country-tbl" class="table table-bordered table-hover">
            <thead>
            <tr>
              <th>#</th>
              <th>Name</th>    
              <th>ShortName</th>         
              <th>Region</th>
              <th>DialCode</th>
              <th>Action</th>                  
            </tr>
            </thead>
            <tbody> 
            @foreach ($data as $i => $country)              
            <tr>
              <td>{{ ($i+1) }}</td>
              <td width="25%">
               
                {{ $country->name }}
              </td>
              <td width="10%">{{ $country->alpha2 }}</td>
              <td>{{ $country->region }}</td>
              <td>{{ $country->dial_code }}</td>
              <td>
                <a class="btn btn-success" href="{{ route('countries.edit',$country->id) }}" title="Edit"><i class="fas fa-edit"></i></a>                
                {!! Form::open(['method' => 'DELETE','route' => ['countries.destroy', $country->id],'style'=>'display:inline']) !!}
                <button type="submit" class="ml-2 btn btn-danger" title="Delete"><i class="fas fa-trash"></i></button>
                {!! Form::close() !!}               
              </td>
            </tr>
             @endforeach  
            </tbody>
            <tfoot>
            <tr>
              <th>#</th>
              <th>Name</th>    
              <th>ShortName</th>         
              <th>Region</th>
              <th>DialCode</th>
              <th>Action</th>                      
            </tr>                  
            </tr>
            </tfoot>
          </table>          
        </div>
        <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
      <!-- <div class="col">&nbsp;</div> -->
    </div>
  </div>
</section> 
<!-- /.content -->
@endsection