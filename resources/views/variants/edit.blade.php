@extends('layouts.admin')


@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <div class="container justify-content-center">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Edit Variant</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
          <li class="breadcrumb-item"><a href="{{ route('variants.index') }}">Variant</a></li>
          <li class="breadcrumb-item active">Edit Variant</li>
        </ol>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container justify-content-center">
      <div class="row">
        <!-- left column -->
     
      <div class="col">
        <!-- general form elements -->
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Edit Variant</h3>
          </div>
          <!-- /.card-header -->
          <!-- form start -->
            {!! Form::model($variant, ['method' => 'PATCH', 'enctype' => 'multipart/form-data', 'route' => ['variants.update', $variant->id]]) !!}          
            <div class="card-body">
                @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                       @foreach ($errors->all() as $error)
                         <li>{{ $error }}</li>
                       @endforeach
                    </ul>
                </div>
                @endif
                <div class="row">
                  <div class="col-6">
                    <div class="form-group">
                      <label for="variant_id">Variant <span class="help-block">*</span></label>
                      <select class="form-control" name="variant_id" id="variant_id">   
                        <option value="">-- Select --</option>                
                        @foreach ($variants as $key => $value)
                          <option value="{{ $key }}" {{ $variant->variant_id == $key? 'selected' : '' }}> 
                              {{ $value }} 
                          </option>
                        @endforeach  
                      </select>    
                    </div>
                  </div>
                  <div class="col-6">
                    <div class="form-group" id="newvar">
                        <label for="v_name">Variant Title <span class="help-block">*</span></label>
                        {!! Form::text('name', null, array('placeholder' => 'Variant Title','class' => 'form-control', 'id' => 'v_name')) !!}
                    </div> 
                  </div>                 
                </div> 
                
                <div class="row">
                  <div class="col-6">
                    <div class="form-group">
                        <label for="voption">Variant Option</label>
                        {!! Form::text('option', null, array('placeholder' => 'Options','class' => 'form-control', 'id' => 'voption')) !!}
                    </div> 
                  </div>  
                  
                  <div class="col-6"></div>               
                </div> 
                
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
              <button type="submit" class="btn btn-primary">Submit</button>
              <a class="btn btn-secondary float-right" href="{{ route('variants.index') }}"> Cancel</a>
            </div>
          {!! Form::close() !!}
        </div>
        <!-- /.card -->
    </div>
    
    </div>
    </div>
</section>
<script type="text/javascript">
$('#variant_id').on('change', function(e){

  let selvar = $(this).find(':selected').text();
  selvar = selvar.trim();    
  if(selvar){
    $('#v_name').val(selvar);
  }

});
</script>
@endsection