@extends('layouts.admin')

@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
  <div class="container justify-content-center">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Create New Variant</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
          <li class="breadcrumb-item"><a href="{{ route('variants.index') }}">Variants</a></li>
          <li class="breadcrumb-item active">Create New Variant</li>
        </ol>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container justify-content-center">
      <div class="row">
        <!-- left column -->
     
      <div class="col">
        <!-- general form elements -->
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">New Variant</h3>
          </div>
          <!-- /.card-header -->
          <!-- form start -->
          {!! Form::open(array('route' => 'variants.store','method'=>'POST', 'enctype' => 'multipart/form-data', 'autocomplete' => 'off' )) !!}
            <div class="card-body">
                @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                       @foreach ($errors->all() as $error)
                         <li>{{ $error }}</li>
                       @endforeach
                    </ul>
                </div>
                @endif
                <div class="row">
                  <div class="col-6">
                    <div class="form-group">
                      <label for="variant_id">Variant <span class="help-block">*</span></label>
                      <select class="form-control" name="variant_id" id="variant_id">   
                        <option value="">-- Select --</option>                
                        @foreach ($variant as $key => $value)
                          <option value="{{ $key }}"> 
                              {{ $value }} 
                          </option>
                        @endforeach  
                        <option value="0"> 
                            ADD NEW (if not)
                        </option>  
                      </select>    
                    </div>
                  </div>
                  <div class="col-6">
                    <div class="form-group d-none" id="newvar">
                        <label for="m_name">Variant Title <span class="help-block">*</span></label>
                        {!! Form::text('name', null, array('placeholder' => 'Variant Title','class' => 'form-control', 'id' => 'm_name')) !!}
                    </div> 
                  </div>                 
                </div> 
                
                <div class="row">
                  <div class="col-6">
                    <div class="form-group">
                        <label for="voption">Variant Option</label>
                        {!! Form::text('option', null, array('placeholder' => 'Options','class' => 'form-control', 'id' => 'voption')) !!}
                    </div> 
                  </div>  
                  
                  <div class="col-6"></div>               
                </div> 
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
              <button type="submit" class="btn btn-primary">Submit</button>
              <a class="btn btn-secondary float-right" href="{{ route('variants.index') }}"> Back</a>
            </div>
          {!! Form::close() !!}
        </div>
        <!-- /.card -->
    </div>
    
    </div>
    </div>
</section>
<script type="text/javascript">
  $('#variant_id').on('change', function(e){
    let varid = $(this).val();

    if(varid == 0){
      $('#newvar').removeClass('d-none');
    }else{
      $('#newvar').addClass('d-none');
    }

  });
</script>
@endsection
