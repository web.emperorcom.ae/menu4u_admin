@extends('layouts.admin')


@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Brand</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
          <li class="breadcrumb-item active">Brand</li>
        </ol>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col">

        <div class="card">
        <div class="card-header">
          <h3 class="card-title">Manage Brand</h3>         
          <a class="btn bg-gradient-success float-right" href="{{ route('brands.create') }}">CREATE NEW</a>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
          @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
          @endif          
          <table id="brand-tbl" class="table table-bordered table-hover">
            <thead>
            <tr>
              <th>#</th>
              <th>Title</th>
              <th>Description</th>
              <th>Image</th>
              <th>Action</th>                  
            </tr>
            </thead>
            <tbody> 
             @foreach ($brand as $key => $ban)
              <tr>
                <td>{{ ($key+1) }}</td>
                <td>{{ $ban->name }}</td>
                <td width="40%">{{ $ban->description }}</td>
                <td>@if($ban->image)
                    <div class="d-inline">    
                      <img src="https://api.menu4u.ae/api/image/{{$ban->image}}" class="img-fluid img-thumbnail m-1 mht-80">
                    </div>
                    @else
                    NO IMAGE
                    @endif
                </td>
                <td>
                  <a class="btn btn-warning mr-2" href="{{ route('brands.edit',$ban->id) }}" title="Edit"><i class="fas fa-edit"></i></a>

                  {!! Form::open(['method' => 'DELETE','route' => ['brands.destroy', $ban->id],'style'=>'display:inline']) !!}
                    <button type="submit" class="btn btn-danger" title="Delete"><i class="fas fa-trash"></i></button>
                  {!! Form::close() !!}                  
                </td>
              </tr>
             @endforeach 
            </tbody>
            <tfoot>
            <tr>
              <th>#</th>
              <th>Title</th>
              <th>Description</th>
              <th>Image</th>
              <th>Action</th>                 
            </tr>
            </tfoot>
          </table>
        </div>
        <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
      <!-- <div class="col">&nbsp;</div> -->
    </div>
  </div>
</section> 
<!-- /.content -->

@endsection