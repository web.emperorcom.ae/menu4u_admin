@extends('layouts.admin')


@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <div class="container justify-content-center">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Edit Measurement</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
          <li class="breadcrumb-item"><a href="{{ route('attributes.index') }}">Measurement</a></li>
          <li class="breadcrumb-item active">Edit Measurement</li>
        </ol>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container justify-content-center">
      <div class="row">
        <!-- left column -->
     
      <div class="col">
        <!-- general form elements -->
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Edit Measurement</h3>
          </div>
          <!-- /.card-header -->
          <!-- form start -->
            {!! Form::model($attribute, ['method' => 'PATCH', 'enctype' => 'multipart/form-data', 'route' => ['attributes.update', $attribute->id]]) !!}          
            <div class="card-body">
                @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                       @foreach ($errors->all() as $error)
                         <li>{{ $error }}</li>
                       @endforeach
                    </ul>
                </div>
                @endif
                 <div class="row">
                  <div class="col">
                    <div class="form-group">
                        <label for="a_name">Title<span class="help-block">*</span></label>
                        {!! Form::text('name', null, array('placeholder' => 'Title','class' => 'form-control', 'id' => 'a_name')) !!}
                    </div> 
                  </div>
                  <div class="col">
                    <div class="form-group">
                      <label for="default_value">Default Value</label>
                      {!! Form::text('default_value', null, array('placeholder' => 'Default Value','class' => 'form-control', 'id' => 'default_value')) !!}
                    </div>  
                  </div>
                </div>
                <div class="row">
                  <div class="col-6">
                    <div class="form-group">
                      <label for="unit">Measurement<span class="help-block">*</span></label>
                      <select class="form-control" name="measurement_id">   
                        <option value="">-- Select --</option>                
                        @foreach ($units as $key => $value)
                          <option value="{{ $key }}" {{$attribute->measurement_id == $key? 'selected' : ''}}> 
                              {{ $value }} 
                          </option>
                        @endforeach    
                    </select>    
                    </div>  
                  </div>
                </div>
                
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
              <button type="submit" class="btn btn-primary">Submit</button>
              <a class="btn btn-secondary float-right" href="{{ route('attributes.index') }}"> Cancel</a>
            </div>
          {!! Form::close() !!}
        </div>
        <!-- /.card -->
    </div>
    
    </div>
    </div>
</section>

@endsection