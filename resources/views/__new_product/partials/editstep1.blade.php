<div class="form-group row">
  <label for="prodname" class="col-sm-2 col-form-label">Product Name <span class="help-block">*</span></label>
  <div class="col-sm-10">
    {!! Form::text('name', null, array('placeholder' => 'Product Name','class' => 'form-control', 'id' => 'prodname')) !!}
  </div>
</div>
<div class="form-group row">
  <label for="description" class="col-sm-2 col-form-label">Description</label>
  <div class="col-sm-10">
    <textarea class="summernote" placeholder="Place some text here" name="description" id="description"
    style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">{{$product->description}}</textarea>
  </div>
</div>                  
<div class="form-group row">
  <label for="status" class="col-sm-2 col-form-label">Product Status</label>
  <div class="col-sm-10">
    <div class="form-group clearfix">
      <div class="icheck-primary d-inline">
        <input type="radio" id="status1" name="is_blocked" value="1" {{ ($product->is_blocked == 1)? 'checked': ''}}>
        <label for="status1">Enabled</label>
      </div>
      <div class="icheck-primary d-inline">
        <input type="radio" id="status2" name="is_blocked" value="0" {{ ($product->is_blocked == 0)? 'checked': ''}}>
        <label for="status2">Disabled</label>
      </div>
    </div>
  </div> 
</div>
<div class="form-group row">
    <label for="price" class="col-sm-2 col-form-label">Price</label>
    <div class="col-sm-10">
      {!! Form::text('price',$product->price, array('placeholder' => 'Price','class' => 'form-control', 'id' => 'price')) !!}
    </div>
</div>
<div class="form-group row">
  <label for="discount" class="col-sm-2 col-form-label">Min Price</label>
  <div class="col-sm-10">
    {!! Form::text('min_price', $product->min_price, array('placeholder' => 'Min Price','class' => 'form-control', 'id' => 'discount')) !!}
  </div>
</div>
<div class="form-group row">
  <label class="col-sm-2 col-form-label">Default Image</label>
  <div class="col-sm-10">
    <div class="input-group">
        <div class="custom-file">
            <input type="file" class="custom-file-input" id="file-input-def" name="image" accept="image/svg,image/jpeg,image/jpg,image/png,image/gif"/>

            <label class="custom-file-label" for="file-input">Default Image</label>
        </div>
        <div class="input-group-append">
            <span class="input-group-text" id="">Upload</span>
        </div>
    </div>
    <p class="small text-muted mt-1"><strong class="help-block">*</strong> Image should be in any of the format (jpeg, png, jpg, gif, svg)</p>
    <div id="file-error-def" class="text-danger mt-1"></div>
    <span class="text-danger">{{ $errors->first('image') }}</span> 
  </div>          
</div>
<div class="form-group">
    <div id="thumb-output-def"></div>
    @if($product->image)
    <div class="d-inline">    
      <img src="https://api.menu4u.ae/api/image/{{$product->image}}" class="img-fluid img-thumbnail m-1 mht-100">
    </div>
    @endif
</div>