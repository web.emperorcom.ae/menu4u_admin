<div class="form-group row">
  <label for="barcode" class="col-sm-2 col-form-label">Barcode</label>
  <div class="col-sm-10">
    {!! Form::text('barcode', null, array('placeholder' => 'Barcode','class' => 'form-control', 'id' => 'barcode')) !!}
  </div>
</div>
<div class="form-group row">
  <label for="origin" class="col-sm-2 col-form-label">Country of Origin</label>
  <div class="col-sm-10">
    <select class="form-control" name="country_id" id="country">
      <option value="">-- Select --</option>                                
      @foreach ($countries as $key => $value)
        <option value="{{ $key }}" {{$product->country_id == $key? 'selected': ''}}> 
            {{ $value }} 
        </option>
      @endforeach
    </select>
  </div>
</div>
<div class="form-group row">
  <label for="brand" class="col-sm-2 col-form-label">Brand</label>
  <div class="col-sm-10">
    <select class="form-control" name="brand_id" id="brand">
      <option value="">-- Select --</option>                                
      @foreach ($brand as $key => $value)
        <option value="{{ $key }}" {{$product->brand_id == $key? 'selected': ''}}> 
            {{ $value }} 
        </option>
      @endforeach
    </select>
  </div> 
</div>
<div class="form-group row">
  <label for="category" class="col-sm-2 col-form-label">Category  <span class="help-block">*</span></label>
  <div class="col-sm-10">
    <select class="form-control" name="category_id" id="category">
      <option value="">-- Select --</option>                                
      @foreach ($categories as $key => $cat)
        <option value="{{ $cat->id }}" {{$product->category_id == $cat->id? 'selected': ''}}> 
          @if($cat->category_id > 0)
            {{ $cat->parent }}  > 
          @endif
          {{ $cat->name }}
        </option>
      @endforeach  
    </select>
  </div> 
</div>
<div class="row">
  <div class="col-sm-12 col-md-12">&nbsp;</div>
</div>
<div class="row">
  <div class="form-group col-2">
    <label for="sku">SKU</label>   
    {!! Form::text('sku', null, array('placeholder' => 'SKU','class' => 'form-control', 'id' => 'sku')) !!}    
  </div>  
  <div class="form-group col-1">
    <label for="price">Price</label>   
      {!! Form::text('price',null, array('placeholder' => 'Price','class' => 'form-control', 'id' => 'price')) !!}
  </div>
  <div class="form-group col-1">
    <label for="quantity">Count</label>
      {!! Form::number('count',  null, array('placeholder' => 'Quantity','class' => 'form-control', 'id' => 'quantity')) !!}
  </div>
  <div class="form-group col-2">
    <label for="reserved_count">Reserved Count</label>
      {!! Form::number('reserved_count',  null, array('placeholder' => 'Reserved Count','class' => 'form-control', 'id' => 'reserved_count')) !!}
  </div>
  <div class="form-group col-2">
    <label for="variant_id">Variants</label>   
    <select class="form-control" name="variant_id" id="variant_id" >
      <option value="">-- Variant --</option> 
      @foreach ($variants as $key => $value)
        <option value="{{ $key }}"> 
            {{ $value }} 
        </option>
      @endforeach 
    </select>
  </div>  
  <div class="form-group col-2">
    <label for="variant_id">Options</label>   
    <select class="form-control" name="variant_option_id" id="variant_option_id" >
      <option value="">-- Variant Options --</option>   
       @foreach ($variant_options as $key => $value)
        <option value="{{ $key }}"> 
            {{ $value }} 
        </option>
      @endforeach                    
    </select>
  </div>                     
  <div class="col-2"><br/>
    <button type="button" class="btn btn-primary" id="item-add"><i class="fas fa-plus"></i></button>
  </div>               
</div>
<div class="row">
  <div class="col-sm-12 col-md-12">&nbsp;</div>
</div>
<div class="table-responsive p-0">
  <table class="table table-striped table-hover text-nowrap" id="tbl-prod-option-item">
    <thead>
      <tr>
        <th scope="col">SKU</th>        
        <th scope="col">Price</th>
        <th scope="col">Count</th>
        <th scope="col">Reserved</th>
        <th scope="col">Variant</th>
        <th scope="col">Options</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
    @foreach($productoptions as $key => $item)
      <tr id="orw-{{$key}}">
        <td>{{ $item->sku }}</td>        
        <td>{{ $item->price }}</td>
        <td>{{ $item->count }}</td>
        <td>{{ $item->reserved_count }}</td>
        <td>{{ $item->variant }}</td>
        <td>{{ $item->variantoption }}</td>
        <td>
        <span id="itm{{$key}}" class="d-none">{{ json_encode($item) }}</span>
        <a href="javascript:void(0);" class="del-orow btn btn-danger" data-id="{{$key}}" title="Delete"><i class="fas fa-trash"></i></a>
        <input type="hidden" id="prod_option_id{{$key}}" value="{{$item->id}}">
        </td>
      </tr>
    @endforeach
    </tbody>
  </table>
</div>
<div class="row">
  <div class="col-sm-12 col-md-12">&nbsp;</div>
</div>