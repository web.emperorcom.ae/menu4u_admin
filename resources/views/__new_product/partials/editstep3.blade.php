<div class="form-group row">
  <label for="measurement" class="col-2 col-form-label">Measurement</label>
  <div class="col-8">
    <select class="form-control" name="measurement_id" id="measurement">
      <option value="">-- Unit --</option>                                
      @foreach ($measurement as $key => $value)
        <option value="{{ $key }}" {{isset($product->measurement_id) && $product->measurement_id == $key? 'selected': ''}}> 
            {{ $value }} 
        </option>
      @endforeach
    </select>
  </div>
</div>
<div class="row">
  <div class="col-12">&nbsp;</div>
</div>
 <div class="form-group row">  
  <div class="col-5">
    <label>Attributes</label>
    <select class="form-control" name="attribute_id" id="attribute_id">
      <option value="">-- Attributes --</option>  
       @foreach ($attributes as $key => $value)
        <option value="{{ $key }}"> 
            {{ $value }} 
        </option>
      @endforeach
    </select>
  </div>
  <div class="col-5">
    <label>Attribute Value</label>
     {!! Form::text('attr_value', null, array('placeholder' => 'Attribute Value','class' => 'form-control', 'id' => 'attr_value')) !!}
  </div>
  <div class="col-2"><br/>
    <button type="button" class="btn btn-primary" id="attribute-add"><i class="fas fa-plus"></i></button>
  </div> 
</div>
<div class="table-responsive p-0">
  <table class="table table-striped table-hover text-nowrap" id="tbl-prod-attr-item">
    <thead>
      <tr>
        <th scope="col">Attributes</th>  
        <th scope="col">Value</th>       
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
    @foreach($productattributes as $key => $item)
      <tr id="arw-{{$key}}">
        <td>{{ $item->attrname }}</td>
        <td>{{ $item->value }}</td>
        <td>
        <span id="itm{{$key}}" class="d-none">{{ json_encode($item) }}</span>
        <a href="javascript:void(0);" class="del-arow btn btn-danger" data-id="{{$key}}" title="Delete"><i class="fas fa-trash"></i></a>
        <input type="hidden" id="prod_attr_id{{$key}}" value="{{$item->id}}">
        </td>
      </tr>
    @endforeach
    </tbody>
  </table>
</div>
<div class="row">
  <div class="col-sm-12 col-md-12">&nbsp;</div>
</div>

<div class="form-group row">
  <div class="col-6">
    <label for="variant_id">Variants</label>
    <select class="form-control" name="variant_id" id="variant_id" >
      <option value="">-- Variant --</option> 
      @foreach ($variants as $key => $value)
        <option value="{{ $key }}"> 
            {{ $value }} 
        </option>
      @endforeach 
      <option value="0">OTHER</option> 
    </select>
  </div>
  <div class="col-6"><br/>
    <button type="button" class="btn btn-primary" id="variant-add"><i class="fas fa-plus"></i></button>
  </div>             
</div>
<div class="row">
  <div class="col-12">&nbsp;</div>
</div>
<div class="table-responsive p-0">
  <table class="table table-striped table-hover text-nowrap" id="tbl-prod-variant-item">
    <thead>
      <tr>
        <th scope="col">Variants</th>        
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
    @foreach($productvariants as $key => $item)
      <tr id="vrw-{{$key}}">
        <td>{{ $item->variant }}</td>
        <td>
        <span id="itm{{$key}}" class="d-none">{{ json_encode($item) }}</span>
        <a href="javascript:void(0);" class="del-vrow btn btn-danger" data-id="{{$key}}" title="Delete"><i class="fas fa-trash"></i></a>
        <input type="hidden" id="prod_variant_id{{$key}}" value="{{$item->variant_id}}">
        </td>
      </tr>
    @endforeach
    </tbody>
  </table>
</div>
<div class="row">
  <div class="col-sm-12 col-md-12">&nbsp;</div>
</div>