@extends('layouts.admin')


@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Edit Product</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
          <li class="breadcrumb-item"><a href="{{ route('products.index') }}">Products</a></li>
          <li class="breadcrumb-item active">Edit Product</li>
        </ol>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>
<!-- Main content -->

<!-- Main content -->
<section class="content">
    <div class="container justify-content-center">
      <div class="row">
        <!-- left column -->
     
      <div class="col">
        <!-- general form elements -->        
        <div class="card">
          <div class="card-header p-2">
            <ul class="nav nav-pills">
              <li class="nav-item"><a class="nav-link active" href="#activity" data-toggle="tab">General</a></li>
              <li class="nav-item"><a class="nav-link" href="#timeline" data-toggle="tab">Inventory</a></li>
              <li class="nav-item"><a class="nav-link" href="#settings" data-toggle="tab">Attributes</a></li>
            </ul>
          </div><!-- /.card-header -->
           {!! Form::model($product, ['method' => 'PATCH', 'enctype' => 'multipart/form-data', 'route' => ['products.update', $product->id]]) !!}   
          <div class="card-body">
            @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                <ul>
                   @foreach ($errors->all() as $error)
                     <li>{{ $error }}</li>
                   @endforeach
                </ul>
            </div>
            @endif            
            <div class="tab-content">
              <div class="active tab-pane" id="activity">
                @include('product.partials.editstep1')
              </div>
              <div class="tab-pane" id="timeline">
                @include('product.partials.editstep2')
              </div>
              <div class="tab-pane" id="settings">
                @include('product.partials.editstep3')
              </div>
            </div>
          </div>
          <!-- card body -->
          <div class="card-footer">
            <button type="submit" class="btn btn-primary">Save</button>
            <a class="btn btn-secondary float-right" href="{{ route('products.index') }}"> Cancel</a>
          </div>
          <!-- card footer -->
          {!! Form::close() !!}
        </div>
        
        <!-- card-->
      </div>
      <!-- col -->
    </div>
    <!-- row -->
  </div>
</section>
<script type="text/javascript"> 
  $(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    // Variant and its options
    $('#variant_id').on ('change', function(e){
      let variant_id = $(this).val();
      let htm = '<option value="">-- Select --</option>';
      if(variant_id){
         $.ajax({
            url:"{{ route('variant.options') }}",
            method:"POST",
            data:{variant_id: variant_id}, 
            success:function(data){
             
              if(data){
                $.each(data, function(index, opt){
                  htm += '<option value="'+index+'">'+opt+'</option>';
                });                
              }
              $('#variant_option_id').html(htm);
            }
        });

      }else{
        $('#variant_option_id').html(htm);
      }
    });
    var attr = [];
    // Unit and its attributes
    $('#measurement').on ('change', function(e){
      let measurement_id = $(this).val();
      let htm = '<option value="">-- Select --</option>';
      if(measurement_id){
         $.ajax({
            url:"{{ route('measurement.attributes') }}",
            method:"POST",
            data:{measurement_id: measurement_id}, 
            success:function(data){
             
              if(data){
                attr = data;
                $.each(data, function(index, opt){
                  htm += '<option value="'+opt.id+'">'+opt.name+'</option>';
                });                
              }
              $('#attribute_id').html(htm);
              $('#attr_value').val('');
            }
        });

      }else{
        $('#attribute_id').html(htm);$('#attr_value').val('');

        if(!$('#attr_value').hasAttr('disabled')){
          $('#attr_value').attr( "disabled", true );
        }
      }
    });

    $('#attribute_id').on ('change', function(e){
      let attrid = $(this).val();
      if(!attrid){
        $('#attr_value').attr( "disabled", true );
      }else{
        $.each(attr, function(index, opt){
          if(opt.id == attrid){ console.log(opt.id, attrid);

            $('#attr_value').removeAttr( "disabled").val(opt.default_value);
          }
        });
      }
      
                  
    });

    // Summernote
    $('.summernote').summernote({
      height: 150,   //set editable area's height
      codemirror: { // codemirror options
        theme: 'monokai'
      }
    });

    $('#file-input').on('change', function(){ //on file input change
        if (window.File && window.FileReader && window.FileList && window.Blob) //check File API supported browser
        {             
            var data = $(this)[0].files; //this file data
            
            $.each(data, function(index, file){ //loop though each file
                if(/(\.|\/)(bmp|jpe?g|png)$/i.test(file.type)){ //check supported file type
                    var fRead = new FileReader(); //new filereader
                    fRead.onload = (function(file){ //trigger function on successful read
                        return function(e) {                           
                          let preview = $('<img/>').addClass('img-fluid img-thumbnail m-1 mht-100').attr('src', e.target.result); //create image element                           
                          $('#thumb-output').append(preview); //append image to output element
                        };
                    })(file);
                    fRead.readAsDataURL(file); //URL representing the file's data.
                    $('#file-error').html("");
                }else{                        
                    $('#file-error').html("Selected file extension not allowed");
                    return;
                }; 
            });
             
        }else{
            alert("Your browser doesn't support File API!"); //if File API is absent
        }
    }); 

    $('#file-input-def').on('change', function(){ //on file input change
        if (window.File && window.FileReader && window.FileList && window.Blob) //check File API supported browser
        {             
            var data = $(this)[0].files; //this file data
            
            $.each(data, function(index, file){ //loop though each file
                if(/(\.|\/)(bmp|jpe?g|png)$/i.test(file.type)){ //check supported file type
                    var fRead = new FileReader(); //new filereader
                    fRead.onload = (function(file){ //trigger function on successful read
                        return function(e) {                           
                          let preview = $('<img/>').addClass('img-fluid img-thumbnail m-1 mht-100').attr('src', e.target.result); //create image element                           
                          $('#thumb-output-def').html(preview); //append image to output element
                        };
                    })(file);
                    fRead.readAsDataURL(file); //URL representing the file's data.
                    $('#file-error-def').html("");
                }else{                        
                    $('#file-error-def').html("Selected file extension not allowed");
                    return;
                }; 
            });
             
        }else{
            alert("Your browser doesn't support File API!"); //if File API is absent
        }
    });   
});
</script>
@endsection