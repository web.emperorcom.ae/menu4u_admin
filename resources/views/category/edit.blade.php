@extends('layouts.admin')


@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Edit Category</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
          <li class="breadcrumb-item"><a href="{{ route('categories.index') }}">Category</a></li>
          <li class="breadcrumb-item active">Edit Category</li>
        </ol>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
      <div class="row">
        <!-- left column -->
     
      <div class="col">
        <!-- general form elements -->
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Edit Category</h3>
          </div>
          <!-- /.card-header -->
          <!-- form start -->
            {!! Form::model($category, ['method' => 'PATCH', 'enctype' => 'multipart/form-data', 'route' => ['categories.update', $category->id]]) !!}          
            <div class="card-body">
                @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                       @foreach ($errors->all() as $error)
                         <li>{{ $error }}</li>
                       @endforeach
                    </ul>
                </div>
                @endif
                <div class="row">
                <div class="form-group col-6">
                    <label for="cat_name">Category Name <span class="help-block">*</span></label>
                    {!! Form::text('name', null, array('placeholder' => 'Category Name','class' => 'form-control', 'id' => 'cat_name')) !!}
                </div> 

                <div class="form-group  col-6">
                    <label>Parent Id:</label>
                    <select class="form-control" name="category_id">   
                      <option value="0">-- Select --</option>                
                      @foreach ($categories as $key => $value)
                        <option value="{{ $key }}" {{$category->category_id == $key? 'selected': ''}}> 
                            {{ $value }} 
                        </option>
                      @endforeach    
                    </select>                    
                </div>   
                </div>  
                <div class="row">
                  <div class="form-group col-12">
                    <label for="description">Description</label>
                    {!! Form::textarea('description', null, ['placeholder' => 'Description','class' => 'form-control','id' => 'description', 'rows' => 4, 'cols' => 54]) !!}
                  </div>
                </div>
                <div class="row">
                <div class="form-group col-6">
                    <label>Upload Image:</label>
                    <div class="input-group">
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="file-input" name="image_path" accept="image/svg,image/jpeg,image/jpg,image/png,image/gif"/>
                            <label class="custom-file-label" for="file-input">Choose file</label>
                        </div>
                        <div class="input-group-append">
                            <span class="input-group-text" id="">Upload</span>
                        </div>
                    </div>
                    <p class="small text-muted mt-1">Image should be in any of the format (jpeg,png,jpg,gif,svg)</p>
                    <div id="file-error" class="text-danger mt-1"></div>
                    <span class="text-danger">{{ $errors->first('image_path') }}</span>           
                </div>
                <div class="form-group  col-6">
                    <div id="thumb-output"></div>

                    @if($category->image)
                    <div class="d-inline">    
                      <img src="https://api.menu4u.ae/api/image/{{$category->image}}" class="img-fluid img-thumbnail m-1 mht-100">
                    </div>
                    @endif
                </div> 
              </div>              
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
              <button type="submit" class="btn btn-primary">Submit</button>
              <a class="btn btn-secondary float-right" href="{{ route('categories.index') }}"> Cancel</a>
            </div>
          {!! Form::close() !!}
        </div>
        <!-- /.card -->
    </div>
    
    </div>
    </div>
</section>

<script type="text/javascript"> 
$(document).ready(function(){
 $('#file-input').on('change', function(){ //on file input change
    if (window.File && window.FileReader && window.FileList && window.Blob) //check File API supported browser
    {
        var data = $(this)[0].files; //this file data
         
        $.each(data, function(index, file){ //loop though each file
            if(/(\.|\/)(gif|jpe?g|png|svg)$/i.test(file.type)){ //check supported file type
                var fRead = new FileReader(); //new filereader
                fRead.onload = (function(file){ //trigger function on successful read
                return function(e) {
                    var img = $('<img/>').addClass('img-fluid img-thumbnail m-1 mht-100').attr('src', e.target.result); //create image element 
                   
                    $('#thumb-output').html(img); //append image to output element
                };
                })(file);
                fRead.readAsDataURL(file); //URL representing the file's data.
                $('#file-error').html("");
            }else{              
                $('#file-error').html("Selected file extension not allowed");
                return;
            };
        });
         
    }else{
        alert("Your browser doesn't support File API!"); //if File API is absent
    }
 });
});
</script>
@endsection