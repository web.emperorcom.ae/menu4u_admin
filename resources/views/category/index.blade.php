@extends('layouts.admin')


@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Category</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
          <li class="breadcrumb-item active">Manage Categories</li>
        </ol>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>
<!-- Main content -->
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col">
        <div class="card">
        <div class="card-header">
          <h3 class="card-title">Manage Categories</h3>          
            <a class="btn bg-gradient-success float-right" href="{{ route('categories.create') }}" title="Add New">CREATE NEW</a>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
          @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
          @endif
          <table id="category-tbl" class="table table-bordered table-hover">
            <thead>
            <tr>
              <th>#</th>
              <th>Name</th>             
              <th>Description</th>
              <th>Image</th>
              <th>Action</th>                  
            </tr>
            </thead>
            <tbody> 
            @foreach ($data as $i => $category)              
            <tr>
              <td>{{ ($i+1) }}</td>
              <td width="25%">
                @if($category->category_id > 0)
                   {{ $category->parent }}<span class="mr-2 ml-2">></span>
                @endif
                {{ $category->name }}
              </td>
              <td width="30%">{{ $category->description }}</td>
               <td>@if($category->image)
                  <div class="d-inline">    
                    <img src="https://api.menu4u.ae/api/image/{{$category->image}}" class="img-fluid img-thumbnail m-1 mht-80">
                  </div>
                  @else
                  NO IMAGE
                  @endif</td>
              <td>
                <a class="btn btn-success" href="{{ route('categories.edit',$category->id) }}" title="Edit"><i class="fas fa-edit"></i></a>                
                {!! Form::open(['method' => 'DELETE','route' => ['categories.destroy', $category->id],'style'=>'display:inline']) !!}
                <button type="submit" class="ml-2 btn btn-danger" title="Delete"><i class="fas fa-trash"></i></button>
                {!! Form::close() !!}               
              </td>
            </tr>
             @endforeach  
            </tbody>
            <tfoot>
            <tr>
              <th>#</th>
              <th>Category Name</th>             
              <th>Description</th>
              <th>Image</th>
              <th>Action</th>                      
            </tr>                  
            </tr>
            </tfoot>
          </table>          
        </div>
        <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
      <!-- <div class="col">&nbsp;</div> -->
    </div>
  </div>
</section> 
<!-- /.content -->
@endsection