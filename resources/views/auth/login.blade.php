@extends('layouts.default')

@section('content')
<div class="login-box">
  <div class="login-logo">
    <a href="/"><b>Admin</b> PANEL</a>
  </div>
  <!-- /.login-logo -->
  <div class="card">
    <div class="card-body login-card-body">
      <p class="login-box-msg">Sign in to start your session</p>
      @if($errors->any())
        @foreach ($errors->all() as $error)
          <div class="text-danger mb-2">{{ $error }}</div>
        @endforeach
      @endif
     <form method="POST" action="{{ route('login') }}" autocomplete="off">
        @csrf
        <div class="input-group mb-3">
           <input id="username" type="username" class="form-control @error('username') is-invalid @enderror" name="username" value="{{ old('username') }}" required autocomplete="off" autofocus>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-user"></span>
            </div>
          </div>
        </div>
        @error('username')
            <div class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </div>
        @enderror
        <div class="input-group mb-3">
          <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="off">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        @error('password')
            <div class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </div>
        @enderror
        <div class="row">
          <div class="col-8">
     <!--        <div class="icheck-primary">
                <input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
              <label for="remember">
                Remember Me
              </label>
            </div> -->
          </div>
          <!-- /.col -->
          <div class="col-4">
            <button type="submit" class="btn btn-primary btn-block">Sign In</button>

          </div>
          <!-- /.col -->
        </div>
      </form>
      <p class="mb-1">
        @if (Route::has('password.request'))
         <a href="{{ route('password.request') }}">I forgot my password</a>
        @endif
      </p>     
    </div>
    <!-- /.login-card-body -->
  </div>
</div>
<!-- /.login-box -->
@endsection
