@extends('layouts.admin')


@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Products</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
          <li class="breadcrumb-item active">Manage Product</li>
        </ol>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>
<!-- Main content -->
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col">
        <div class="card">
        <div class="card-header">
          <h3 class="card-title">Manage Products</h3>          
            
            <a class="btn bg-gradient-danger float-right delete-all" href="javascript:void(0);" title="Delete All">DELETE ALL</a>  
            <a class="btn bg-gradient-success float-right mr-3" href="{{ route('products.create') }}" title="Add New">CREATE NEW</i></a>    
        </div>
        <!-- /.card-header -->
        <div class="card-body">
          @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
          @endif
          <table id="product-tbl" class="table table-bordered table-hover">
            <thead>
            <tr>
              <th><input type="checkbox" id="check_all" >
              </th>
              <th>Image</th>
              <th>Name</th>  
              <th>Price (AED)</th>
              <th>Category</th>
              <th>Brand</th>
              <th>Created At</th>
              <th>Action</th>                  
            </tr>
            </thead>
            <tbody> 
            @foreach ($products as $i => $prod)
              @php
                $time = strtotime($prod->created_at) + 60*60*4;
              @endphp               
            <tr id="tr_{{$prod->id}}">
              <td><input type="checkbox" class="checkbox" data-id="{{$prod->id}}"></td>
              <td>@if($prod->image)
                <img src="https://api.menu4u.ae/api/image/{{$prod->image}}" class="img-fluid img-thumbnail m-1" style="max-height: 50px;">
                @else
                No Image
                @endif

              </td>
              <td>{{ $prod->name }}</td>
              <td>{{ $prod->price }}</td>
              <td>{{ $prod->category }}</td>
              <td>{{ $prod->brand }}</td>              
              <td>{{ date("d-m-Y H:i:s", $time) }}</td>                    
              <td>
                <a class="btn btn-success" href="{{ route('products.edit',$prod->id) }}" title="Edit"><i class="fas fa-edit"></i></a>                
                {!! Form::open(['method' => 'DELETE','route' => ['products.destroy', $prod->id],'style'=>'display:inline']) !!}
                <button type="submit" class="ml-2 btn btn-danger" title="Delete"><i class="fas fa-trash"></i></button>
                {!! Form::close() !!}               
              </td>
            </tr>
             @endforeach  
            </tbody>
            <tfoot>
            <tr>
              <th>#</th>
              <th>Image</th>
              <th>Name</th>  
              <th>Price (AED)</th>
              <th>Category</th>
              <th>Brand</th>
              <th>Created At</th>
              <th>Action</th>                      
            </tr>                  
            </tr>
            </tfoot>
          </table>          
        </div>
        <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
      <!-- <div class="col">&nbsp;</div> -->
    </div>
  </div>
</section> 
<!-- /.content -->

<script type="text/javascript">
$(document).ready(function () {

  $('#check_all').on('click', function(e) {
   if($(this).is(':checked',true))  
   {
      $(".checkbox").prop('checked', true);  
   } else {  
      $(".checkbox").prop('checked',false);  
   }
  });

  $('.checkbox').on('click',function(){
    if($('.checkbox:checked').length == $('.checkbox').length){
        $('#check_all').prop('checked',true);
    }else{
        $('#check_all').prop('checked',false);
    }
  });

  $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });

  $('.delete-all').on('click', function(e) {

      var idsArr = [];  

      $(".checkbox:checked").each(function() {  
          idsArr.push($(this).attr('data-id'));
      });  



      if(idsArr.length <=0)  
      {  

          alert("Please select atleast one record to delete.");  

      }  else {  

          if(confirm("Are you sure, you want to delete the selected products?")){ 
              var strIds = idsArr.join(","); 
              $.ajax({

                  url: "{{ route('products.multipledelete') }}",

                  type: 'DELETE',

         //         headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},

                  data: 'ids='+strIds,

                  success: function (data) {
                      if (data.status == true) {
                          $(".checkbox:checked").each(function() {  
                              $(this).parents("tr").remove();

                          });

                          //alert(data['message']);
                          toastr.success(data.message); 

                      } else {

                          // alert('Whoops Something went wrong!!');
                          toastr.error(data.message); 

                      }
                  },
                  error: function (data) {
                    //  alert(data.responseText);
                    toastr.error(data.responseJSON.errors); 
                  }

              });

          }  

      }  

  });

    });

</script>
@endsection