@extends('layouts.admin')


@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Create Product</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
          <li class="breadcrumb-item"><a href="{{ route('products.index') }}">Products</a></li>
          <li class="breadcrumb-item active">Create Product</li>
        </ol>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>
<!-- Main content -->

<!-- Main content -->
<section class="content">
    <div class="container justify-content-center">
      <div class="row">
        <!-- left column -->
     
      <div class="col">
        <!-- general form elements -->        
        <div class="card">
          <div class="card-header p-2">
            <ul class="nav nav-pills">
              <li class="nav-item"><a class="nav-link active" href="#activity" data-toggle="tab">General</a></li>
              <li class="nav-item"><a class="nav-link" href="#timeline" data-toggle="tab">Inventory</a></li>
              <li class="nav-item"><a class="nav-link" href="#settings" data-toggle="tab">Links</a></li>
            </ul>
          </div><!-- /.card-header -->
          {!! Form::open(array('route' => 'products.store','method'=>'POST', 'enctype' => 'multipart/form-data', 'autocomplete' => 'off', 'class' => 'form-horizontal')) !!}
          <div class="card-body">
            @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                <ul>
                   @foreach ($errors->all() as $error)
                     <li>{{ $error }}</li>
                   @endforeach
                </ul>
            </div>
            @endif            
            <div class="tab-content">
              <div class="active tab-pane" id="activity">
              <!-- <form class="form-horizontal"> -->
                <div class="form-group row">
                  <label for="prodname" class="col-sm-2 col-form-label">Product Name <span class="help-block">*</span></label>
                  <div class="col-sm-10">
                    {!! Form::text('name', null, array('placeholder' => 'Product Name','class' => 'form-control', 'id' => 'prodname')) !!}
                  </div>
                </div>
                <div class="form-group row">
                  <label for="description" class="col-sm-2 col-form-label">Description</label>
                  <div class="col-sm-10">
                    <textarea class="summernote" placeholder="Place some text here" name="description" id="description"
                    style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                  </div>
                </div>                  
                <div class="form-group row">
                  <label for="status" class="col-sm-2 col-form-label">Product Blocked</label>
                  <div class="col-sm-10">
                    <div class="form-group clearfix">
                      <div class="icheck-primary d-inline">
                        <input type="radio" id="status1" name="is_blocked" value="1" checked="">
                        <label for="status1">Enabled</label>
                      </div>
                      <div class="icheck-primary d-inline">
                        <input type="radio" id="status2" name="is_blocked" value="0">
                        <label for="status2">Disabled</label>
                      </div>
                    </div>
                  </div> 
                </div>
                <div class="form-group row">
                  <label class="col-sm-2 col-form-label">Default Image</label>
                  <div class="col-sm-10">
                    <div class="input-group">
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="file-input-def" name="image" accept="image/svg,image/jpeg,image/jpg,image/png,image/gif"/>

                            <label class="custom-file-label" for="file-input">Default Image</label>
                        </div>
                        <div class="input-group-append">
                            <span class="input-group-text" id="">Upload</span>
                        </div>
                    </div>
                    <p class="small text-muted mt-1"><strong class="help-block">*</strong> Image should be in any of the format (jpeg, png, jpg, gif, svg)</p>
                    <div id="file-error-def" class="text-danger mt-1"></div>
                    <span class="text-danger">{{ $errors->first('image') }}</span> 
                  </div>          
                </div>
                <div class="form-group">
                    <div id="thumb-output-def"></div>
                </div>
              </div>
              <div class="tab-pane" id="timeline">

                
                <div class="form-group row">
                  <label for="price" class="col-sm-2 col-form-label">Price</label>
                  <div class="col-sm-10">
                    {!! Form::text('price', 0, array('placeholder' => 'Price','class' => 'form-control', 'id' => 'price')) !!}
                  </div>
                </div>
                <div class="form-group row">
                  <label for="min_price" class="col-sm-2 col-form-label">Min Price</label>
                  <div class="col-sm-10">
                    {!! Form::text('min_price', 0, array('placeholder' => 'Min Price','class' => 'form-control', 'id' => 'min_price')) !!}
                  </div>
                </div>
               
                <div class="form-group row">
                  <label for="quantity" class="col-sm-2 col-form-label">Count</label>
                  <div class="col-sm-10">
                    {!! Form::text('count', 0, array('placeholder' => 'Count','class' => 'form-control', 'id' => 'quantity')) !!}
                  </div>
                </div>
                <div class="form-group row">
                  <label for="reserved_count" class="col-sm-2 col-form-label">Reserved Count</label>
                  <div class="col-sm-10">
                    {!! Form::text('reserved_count', 0, array('placeholder' => 'Reserved Count','class' => 'form-control', 'id' => 'reserved_count')) !!}
                  </div>
                </div>
                <div class="form-group row">
                  <label for="barcode" class="col-sm-2 col-form-label">Barcode</label>
                  <div class="col-sm-10">
                    {!! Form::text('barcode', null, array('placeholder' => 'Barcode','class' => 'form-control', 'id' => 'barcode')) !!}
                  </div>
                </div>
                <div class="form-group row">
                  <label for="sku" class="col-sm-2 col-form-label">SKU</label>
                  <div class="col-sm-10">
                    {!! Form::text('sku', null, array('placeholder' => 'SKU','class' => 'form-control', 'id' => 'sku')) !!}
                  </div>
                </div> 
                <div class="form-group row">
                  <label for="variant_id" class="col-sm-2 col-form-label">Variants & Options</label>
                  <div class="col-sm-5">
                    <select class="form-control" name="variant_id" id="variant_id" >
                      <option value="">-- Variant --</option> 
                      @foreach ($variants as $key => $value)
                        <option value="{{ $key }}"> 
                            {{ $value }} 
                        </option>
                      @endforeach  
                      <option value="0">OTHER</option>
                    </select>
                  </div>                 
                  <div class="col-sm-5">
                    <select class="form-control" name="variant_option_id" id="variant_option_id" >
                      <option value="">-- Variant Options --</option>                      
                    </select>
                  </div> 
                </div>              
              </div>
              <div class="tab-pane" id="settings">
                <div class="form-group row">
                  <label for="brand" class="col-sm-2 col-form-label">Brand</label>
                  <div class="col-sm-10">
                    <select class="form-control" name="brand_id" id="brand">
                      <option value="">-- Select --</option>                                
                      @foreach ($brand as $key => $value)
                        <option value="{{ $key }}"> 
                            {{ $value }} 
                        </option>
                      @endforeach
                    </select>
                  </div> 
                </div>
                <div class="form-group row">
                  <label for="category" class="col-sm-2 col-form-label">Category  <span class="help-block">*</span></label>
                  <div class="col-sm-10">
                    <select class="form-control" name="category_id" id="category">
                      <option value="">-- Select --</option>                                
                      @foreach ($categories as $key => $cat)
                        <option value="{{ $cat->id }}"> 
                          @if($cat->category_id > 0)
                            {{ $cat->parent }}  > 
                          @endif
                          {{ $cat->name }}
                        </option>
                      @endforeach  
                    </select>
                  </div> 
                </div>
              
                <div class="form-group row">
                  <label for="origin" class="col-sm-2 col-form-label">Country of Origin</label>
                  <div class="col-sm-10">
                    <select class="form-control" name="country_id" id="country">
                      <option value="">-- Select --</option>                                
                      @foreach ($countries as $key => $value)
                        <option value="{{ $key }}"> 
                            {{ $value }} 
                        </option>
                      @endforeach
                    </select>
                  </div>
                </div>              
                <div class="form-group row">
                  <label for="measurement" class="col-2 col-form-label">Measurement</label>
                  <div class="col-10">
                    <select class="form-control" name="measurement_id" >
                      <option value="">-- Unit --</option>                                
                      @foreach ($measurement as $key => $value)
                        <option value="{{ $key }}"> 
                            {{ $value }} 
                        </option>
                      @endforeach
                    </select>
                  </div>
                </div>
                @for ($i = 0; $i < count($attributes); $i++)
                 <div class="form-group row">
                  <div class="col-6">
                    <select class="form-control" name="attribute_id[]">
                      <option value="">-- Attributes --</option>  
                        @foreach ($attributes as $key => $value)
                        <option value="{{ $key }}"> 
                            {{ $value }}
                        </option>
                        @endforeach
                    </select>
                  </div>
                  <div class="col-6">
                    <input type="text" class="form-control" name="attr_value[]" id="attr_value" placeholder="Attribute Value">
                  </div>
                </div>
                @endfor
              </div>
            </div>
          </div>
          <!-- card body -->
          <div class="card-footer">
            <button type="submit" class="btn btn-primary">Save</button>
            <a class="btn btn-secondary float-right" href="{{ route('products.index') }}"> Cancel</a>
          </div>
          <!-- card footer -->
          {!! Form::close() !!}
        </div>
        
        <!-- card-->
      </div>
      <!-- col -->
    </div>
    <!-- row -->
  </div>
</section>
<script type="text/javascript"> 
  $(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    // Variant and its options
    $('#variant_id').on ('change', function(e){
      let variant_id = $(this).val();
      let htm = '<option value="">-- Select --</option>';
      if(variant_id){
         $.ajax({
            url:"https://admin.menu4u.ae/variantoptions",
            method:"POST",
            data:{variant_id: variant_id}, 
            success:function(data){
             
              if(data){
                $.each(data, function(index, opt){
                  htm += '<option value="'+index+'">'+opt+'</option>';
                });                
              }
              $('#variant_option_id').html(htm);
            }
        });

      }else{
        $('#variant_option_id').html(htm);
      }
    });
    // var attr = [];
    // // Unit and its attributes
    // $('#measurement').on ('change', function(e){
    //   let measurement_id = $(this).val();
    //   let htm = '<option value="">-- Select --</option>';
    //   if(measurement_id){
    //      $.ajax({
    //         url:"{{ route('measurement.attributes') }}",
    //         method:"POST",
    //         data:{measurement_id: measurement_id}, 
    //         success:function(data){
             
    //           if(data){
    //             attr = data;
    //             $.each(data, function(index, opt){
    //               htm += '<option value="'+opt.id+'">'+opt.name+'</option>';
    //             });                
    //           }
    //           $('#attribute_id').html(htm);
    //         }
    //     });

    //   }else{
    //     $('#attribute_id').html(htm);

    //     if(!$('#attr_value').hasAttr('disabled')){
    //       $('#attr_value').attr( "disabled", true );
    //     }
    //   }
    // });

    // $('#attribute_id').on ('change', function(e){
    //   let attrid = $(this).val();
    //   if(!attrid){
    //     $('#attr_value').attr( "disabled", true );
    //   }else{
    //     $.each(attr, function(index, opt){
    //       if(opt.id == attrid){ console.log(opt.id, attrid);

    //         $('#attr_value').removeAttr( "disabled").val(opt.default_value);
    //       }
    //     });
    //   }
      
                  
    // });

    // Summernote
    $('.summernote').summernote({
      height: 150,   //set editable area's height
      codemirror: { // codemirror options
        theme: 'monokai'
      }
    });

    $('#file-input').on('change', function(){ //on file input change
        if (window.File && window.FileReader && window.FileList && window.Blob) //check File API supported browser
        {             
            var data = $(this)[0].files; //this file data
            
            $.each(data, function(index, file){ //loop though each file
                if(/(\.|\/)(bmp|jpe?g|png)$/i.test(file.type)){ //check supported file type
                    var fRead = new FileReader(); //new filereader
                    fRead.onload = (function(file){ //trigger function on successful read
                        return function(e) {                           
                          let preview = $('<img/>').addClass('img-fluid img-thumbnail m-1 mht-100').attr('src', e.target.result); //create image element                           
                          $('#thumb-output').append(preview); //append image to output element
                        };
                    })(file);
                    fRead.readAsDataURL(file); //URL representing the file's data.
                    $('#file-error').html("");
                }else{                        
                    $('#file-error').html("Selected file extension not allowed");
                    return;
                }; 
            });
             
        }else{
            alert("Your browser doesn't support File API!"); //if File API is absent
        }
    }); 

    $('#file-input-def').on('change', function(){ //on file input change
        if (window.File && window.FileReader && window.FileList && window.Blob) //check File API supported browser
        {             
            var data = $(this)[0].files; //this file data
            
            $.each(data, function(index, file){ //loop though each file
                if(/(\.|\/)(bmp|jpe?g|png)$/i.test(file.type)){ //check supported file type
                    var fRead = new FileReader(); //new filereader
                    fRead.onload = (function(file){ //trigger function on successful read
                        return function(e) {                           
                          let preview = $('<img/>').addClass('img-fluid img-thumbnail m-1 mht-100').attr('src', e.target.result); //create image element                           
                          $('#thumb-output-def').html(preview); //append image to output element
                        };
                    })(file);
                    fRead.readAsDataURL(file); //URL representing the file's data.
                    $('#file-error-def').html("");
                }else{                        
                    $('#file-error-def').html("Selected file extension not allowed");
                    return;
                }; 
            });
             
        }else{
            alert("Your browser doesn't support File API!"); //if File API is absent
        }
    });   
});
</script>
@endsection