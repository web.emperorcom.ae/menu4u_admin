@extends('layouts.admin')


@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <div class="container justify-content-center">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Edit Measurement</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
          <li class="breadcrumb-item"><a href="{{ route('measurements.index') }}">Measurement</a></li>
          <li class="breadcrumb-item active">Edit Measurement</li>
        </ol>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container justify-content-center">
      <div class="row">
        <!-- left column -->
     
      <div class="col">
        <!-- general form elements -->
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Edit Measurement</h3>
          </div>
          <!-- /.card-header -->
          <!-- form start -->
            {!! Form::model($measurement, ['method' => 'PATCH', 'enctype' => 'multipart/form-data', 'route' => ['measurements.update', $measurement->id]]) !!}          
            <div class="card-body">
                @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                       @foreach ($errors->all() as $error)
                         <li>{{ $error }}</li>
                       @endforeach
                    </ul>
                </div>
                @endif
                <div class="row">
                  <div class="col">
                    <div class="form-group">
                        <label for="m_name">Measurement Title <span class="help-block">*</span></label>
                        {!! Form::text('name', null, array('placeholder' => 'Measurement Title','class' => 'form-control', 'id' => 'm_name')) !!}
                    </div> 
                  </div>
                  <div class="col">
                    <div class="form-group">
                        <label for="short_name">Short Name<span class="help-block">*</span></label>
                        {!! Form::text('short_name', null, array('placeholder' => 'Short Name','class' => 'form-control', 'id' => 'short_name')) !!}
                    </div>  
                  </div>
                </div>
                <div class="row">
                  <div class="col-6">
                    <div class="form-group">
                        <label for="unit">Unit</label>
                        {!! Form::number('unit', null, array('placeholder' => 'Unit','class' => 'form-control', 'id' => 'unit', 'step' => '0.1')) !!}
                    </div>  
                  </div>
                </div>
                
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
              <button type="submit" class="btn btn-primary">Submit</button>
              <a class="btn btn-secondary float-right" href="{{ route('measurements.index') }}"> Cancel</a>
            </div>
          {!! Form::close() !!}
        </div>
        <!-- /.card -->
    </div>
    
    </div>
    </div>
</section>

@endsection