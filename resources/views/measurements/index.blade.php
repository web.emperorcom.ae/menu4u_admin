@extends('layouts.admin')


@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Measurement</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
          <li class="breadcrumb-item active">Measurement</li>
        </ol>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col">

        <div class="card">
        <div class="card-header">
          <h3 class="card-title">Manage Measurement</h3>         
          <a class="btn bg-gradient-success float-right" href="{{ route('measurements.create') }}">CREATE NEW</a>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
          @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
          @endif          
          <table id="measurement-tbl" class="table table-bordered table-hover">
            <thead>
            <tr>
              <th>#</th>
              <th>Title</th>
              <th>ShortName</th>
              <th>Unit</th>
              <th>Action</th>                  
            </tr>
            </thead>
            <tbody> 
             @foreach ($measurement as $key => $unit)
              <tr>
                <td>{{ ($key+1) }}</td>
                <td>{{ $unit->name }}</td>
                <td>{{ $unit->short_name }}</td>
                <td>{{ $unit->unit }}
                </td>
                <td>
                  <a class="btn btn-warning mr-2" href="{{ route('measurements.edit',$unit->id) }}" title="Edit"><i class="fas fa-edit"></i></a>

                  {!! Form::open(['method' => 'DELETE','route' => ['measurements.destroy', $unit->id],'style'=>'display:inline']) !!}
                    <button type="submit" class="btn btn-danger" title="Delete"><i class="fas fa-trash"></i></button>
                  {!! Form::close() !!}                  
                </td>
              </tr>
             @endforeach 
            </tbody>
            <tfoot>
            <tr>
              <th>#</th>
              <th>Title</th>
              <th>ShortName</th>
              <th>Unit</th>
              <th>Action</th>                 
            </tr>
            </tfoot>
          </table>
        </div>
        <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
      <!-- <div class="col">&nbsp;</div> -->
    </div>
  </div>
</section> 
<!-- /.content -->

@endsection