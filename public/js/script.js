$(document).ready(function(){

	/** add active class and stay opened when selected */
	var url = window.location;

	// for sidebar menu entirely but not cover treeview
	$('ul.nav-sidebar a').filter(function() {
	    return this.href == url;
	}).addClass('active');

	// for treeview
	$('ul.nav-treeview a').filter(function() {
	    return this.href == url;
	}).parentsUntil(".nav-sidebar > .nav-treeview").addClass('menu-open').prev('a').addClass('active');

	$('#country-tbl').DataTable({
    "responsive": true,
    "autoWidth": false,
    "pageLength": 100
  });
  
  $('#variant-tbl').DataTable({
    "responsive": true,
    "autoWidth": false,
    "pageLength": 25
  });

  $('#measurement-tbl').DataTable({
		"responsive": true,
		"autoWidth": false,
    "pageLength": 25
  });
  $('#brand-tbl').DataTable({
    "responsive": true,
    "autoWidth": false,
    "pageLength": 25
  });
  $('#category-tbl').DataTable({
    "responsive": true,
    "autoWidth": false,
    "pageLength": 25
  });
  $('#product-tbl').DataTable({
    "responsive": true,
    "autoWidth": false,
    "pageLength": 25
  });
  $('#order-tbl').DataTable({
    "responsive": true,
    "autoWidth": false,
     "pageLength": 25,
      "order": [[ 0, "desc" ]],
  });
  $('#customer-tbl').DataTable({
    "responsive": true,
    "autoWidth": false,
    "pageLength": 25,
    "order": [[ 0, "desc" ]],
  });
      //Date range as a button
    $('#daterange-btn').daterangepicker(
      {  

        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },

      function (start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
        $('#start_date').val( start.format('YYYY-MM-DD'));
        $('#end_date').val( end.format('YYYY-MM-DD'));

      },
    );
});